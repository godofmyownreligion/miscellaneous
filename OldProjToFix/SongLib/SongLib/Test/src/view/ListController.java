/* Submitted by: Vijay Challa (VKC17), Arvind Vasudevan (AV614)
 * Assignment: Song Library Design & Implementation with GUI
 * Grader: Blerta
 * */

package view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Song;

public class ListController {

	@FXML
	ListView<String> listView;
	@FXML
	TextField inputName;
	@FXML
	TextField inputYear;
	@FXML
	TextField inputAlbum;
	@FXML
	TextField inputArtist;
	@FXML
	Button deleteButton;
	@FXML
	Button addButton;

	String myName;
	String myAlbum;
	String myArtist;
	String myYear;

	// added edit form elements
	@FXML
	TextField editinputName;
	@FXML
	TextField editinputYear;
	@FXML
	TextField editinputAlbum;
	@FXML
	TextField editinputArtist;

	@FXML
	Button editButton;

	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";

	// CSV file header
	private static final String FILE_HEADER = "Name,Album,Artist,Year";
	private ObservableList<String> obsList;
	private ArrayList<Song> songList;

	File fileName = new File("src/resources/songlist.txt");

	private static final int SONG_NAME = 0;
	private static final int SONG_ARTIST = 1;
	private static final int SONG_ALBUM = 2;
	private static final int SONG_YEAR = 3;

	public void start(Stage mainStage) {
		// create an ObservableList
		// from an ArrayList
		obsList = FXCollections.observableArrayList();
		songList = new ArrayList<Song>();
		if (fileName.exists()) {
			loadSongList();
		}
		// select the first item
		listView.getSelectionModel().select(0);

		// set listener for the items
		listView.getSelectionModel().selectedIndexProperty()
				.addListener((obs, oldVal, newVal) -> showItemInputDialog(mainStage));

	}

	private void loadSongList() {

		BufferedReader fileReader = null;

		try {

			// Create a new list of student to be filled by CSV file data
			ArrayList<Song> loadSongs = new <Song>ArrayList();

			String line = "";

			// Create the file reader
			fileReader = new BufferedReader(new FileReader(fileName));

			// Read the CSV file header to skip it
			fileReader.readLine();

			// Read the file line by line starting from the second line
			while ((line = fileReader.readLine()) != null) {
				// Get all tokens available in line
				String[] tokens = line.split(COMMA_DELIMITER);
				if (tokens.length > 0) {
					// Create a new student object and fill his data
					Song student = new Song((tokens[SONG_NAME]), tokens[SONG_ARTIST], tokens[SONG_ALBUM],
							tokens[SONG_YEAR]);
					loadSongs.add(student);
				}
			}

			// Print the new student list
			songList = loadSongs;
		} catch (Exception e) {
			System.out.println("Error in CsvFileReader !!!");
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				System.out.println("Error while closing fileReader !!!");
				e.printStackTrace();
			}
		}

	}

	private void populateObsList() {

		ObservableList<String> newobsList = FXCollections.observableArrayList();

		Collections.sort(songList);

		for (int i = 0; i < songList.size(); i++) {
			newobsList.add(songList.get(i).toString());
		}

		obsList = newobsList;
		listView.setItems(obsList);

		FileWriter fileWriter = null;

		try {
			fileWriter = new FileWriter(fileName);

			// Write the CSV file header
			fileWriter.append(FILE_HEADER.toString());

			// Add a new line separator after the header
			fileWriter.append(NEW_LINE_SEPARATOR);

			// Write a new student object list to the CSV file
			for (Song student : songList) {
				fileWriter.append(String.valueOf(student.getName()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(student.getArtist());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(student.getAlbum());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(student.getYear());
				fileWriter.append(NEW_LINE_SEPARATOR);
			}

			// System.out.println("CSV file was created successfully !!!");

		} catch (Exception e) {
			// System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {

			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				// System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}

		}
	}

	private void showItem(Stage mainStage) {

		String content = "Index: " + listView.getSelectionModel().getSelectedIndex() + "\nValue: "
				+ listView.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.INFORMATION);
		// alert.initModality(Modality.NONE);
		alert.initOwner(mainStage);
		alert.setTitle("List Item");
		alert.setHeaderText("Selected list item properties");

		alert.setContentText(content);
		alert.showAndWait();
		// alert.show();
		// System.out.println("hey there!");
	}

	@FXML
	private void addSong() {
		ArrayList<Song> newSongList = songList;

		myName = inputName.getText();
		myArtist = inputArtist.getText();
		myAlbum = inputAlbum.getText();
		myYear = inputYear.getText();
		Song toAdd = new Song(myName, myArtist, myAlbum, myYear);

		newSongList.add(toAdd);

		songList = newSongList;
		populateObsList();
	}

	// populates edit form with values on select
	private void populateEditSong(int editIndex) {
		Song s = songList.get(editIndex);
		editinputName.setText(s.getName());
		editinputArtist.setText(s.getArtist());
		editinputAlbum.setText(s.getAlbum());
		editinputYear.setText(s.getYear());
	}

	@FXML
	private void editSong() {
		String item = listView.getSelectionModel().getSelectedItem();
		int index = listView.getSelectionModel().getSelectedIndex();
		songList.remove(index);

		ArrayList newSongList = songList;
		myName = editinputName.getText();
		myArtist = editinputArtist.getText();
		myAlbum = editinputAlbum.getText();
		myYear = editinputYear.getText();
		Song toAdd = new Song(myName, myArtist, myAlbum, myYear);

		newSongList.add(index, toAdd);
		songList = newSongList;
		populateObsList();

	}

	@FXML
	private void removeSong() {
		String item = listView.getSelectionModel().getSelectedItem();
		int index = listView.getSelectionModel().getSelectedIndex();
		songList.remove(index);
		if (songList.isEmpty()) {

			listView.getSelectionModel().select(0);
		} else {

			listView.getSelectionModel().select(index);
		}
		populateEditSong(index);
		populateObsList();
	}

	private void showItemInputDialog(Stage mainStage) {
		String item = listView.getSelectionModel().getSelectedItem();
		int index = listView.getSelectionModel().getSelectedIndex();
		populateEditSong(index);

//			TextInputDialog dialog = new TextInputDialog(item);
//			dialog.initOwner(mainStage);
//			dialog.setTitle("List Item");
//			dialog.setHeaderText("Selected Item (Index: " + index + ")");
//			dialog.setContentText("Enter name: ");
//
//			Optional<String> result = dialog.showAndWait();
//			if (result.isPresent()) {
//				obsList.set(index, result.get());
//			}
	}
}
