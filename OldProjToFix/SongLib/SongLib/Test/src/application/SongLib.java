/* Submitted by: Vijay Challa (VKC17), Arvind Vasudevan (AV614)
 * Assignment: Song Library Design & Implementation with GUI
 * Grader: Blerta
 * */

package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import view.ListController;

public class SongLib extends Application {
	@Override
	public void start(Stage primaryStage) 
	throws IOException {
		FXMLLoader loader = new FXMLLoader();   
		loader.setLocation(
				getClass().getResource("/view/List.fxml"));
		AnchorPane root = (AnchorPane)loader.load();


		ListController listController = loader.getController();
		listController.start(primaryStage);

		Scene scene = new Scene(root, 1200, 750);
		primaryStage.setScene(scene);
		primaryStage.show(); 
	}

	public static void main(String[] args) {
		launch(args);
	}

}
