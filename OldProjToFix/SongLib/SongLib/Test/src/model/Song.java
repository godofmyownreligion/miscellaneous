/* Submitted by: Vijay Challa (VKC17), Arvind Vasudevan (AV614)
 * Assignment: Song Library Design & Implementation with GUI
 * Grader: Blerta
 * */

package model;

public class Song implements Comparable<Song> {
	String Name;
	String Artist;
	String Album;
	String ReleaseYear;

	public Song(String newName, String newArtist, String newAlbum, String newReleaseYear) {
		Name = newName;
		Artist = newArtist;
		Album = newAlbum;
		ReleaseYear = newReleaseYear;

	}
	public String getName() {
		return  Name;
	}
	public String getArtist() {
		return  Artist;
	}
	public String getAlbum() {
		return Album;
	}
	public String getYear() {
		return ReleaseYear;
	}

	public String toString() {
		return Name + "	BY " + Artist;
	}
	
	@Override
	public int compareTo(Song other) {
		if(this.Name.compareToIgnoreCase(other.Name) < 0) {
			return -1;
		} else if(this.Name.compareToIgnoreCase(other.Name) > 0) {
			return 1;
		} else {
			if(this.Artist.compareToIgnoreCase(other.Artist) < 0) {
				return -1;
			} else if(this.Artist.compareToIgnoreCase(other.Artist) > 0) {
				return 1;
			}
		}
		return 0;
	}
}
