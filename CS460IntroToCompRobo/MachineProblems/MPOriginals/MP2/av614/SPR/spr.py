import sys
import numpy as np
from collections import defaultdict

'''
Report reflexive vertices
'''
def findReflexiveVertices(polygons):
    # Your code goes here
    # You should return a list of (x,y) values as lists, i.e.
    # vertices = [[x1,y1],[x2,y2],...]

    vertices=[]
    for obstacle in polygons:
        
        for vertex in obstacle:
            currentV = vertex
            passV=False

            if obstacle.index(vertex) == 0:
                prevV = obstacle[len(obstacle)-1]
                nextV = obstacle[1]
            elif obstacle.index(vertex) == len(obstacle) - 1:
                prevV = obstacle[len(obstacle)-2]
                nextV = obstacle[0]
            else:
                prevV=obstacle[obstacle.index(vertex)-1]
                nextV=obstacle[obstacle.index(vertex)+1]
                if currentV[0]<prevV[0] and currentV[1]<prevV[1]:
                    passV=True
                
            if passV==True:
                break   

            currentV = vertex
          
            lenPrev= np.sqrt(((currentV[0]-prevV[0])**2)+((currentV[1]-prevV[1])**2))
            lenNext= np.sqrt(((nextV[0]-currentV[0])**2)+((nextV[1]-currentV[1])**2))
            lenC= np.sqrt(((prevV[0]-nextV[0])**2)+((prevV[1]-nextV[1])**2))
            
            cosAngleV= (((lenNext**2)+(lenPrev**2)-(lenC**2))/(2*lenPrev*lenNext))
            angleV=(np.pi*2)-np.arccos(cosAngleV)
         
            

            if (angleV>np.pi):
                vertices.append(vertex)
            else:
                break
          
            
    
    #print(len(vertices))
    return vertices

'''
Compute the roadmap graph
'''
def computeSPRoadmap(polygons, reflexVertices):
      # Your code goes here
    # You should check for each pair of vertices whether the
    # edge between them should belong to the shortest path
    # roadmap. 
    #
    # Your vertexMap should look like
    # {1: [5.2,6.7], 2: [9.2,2.3], ... }
    #
    # and your adjacencyListMap should look like
    # {1: [[2, 5.95], [3, 4.72]], 2: [[1, 5.95], [5,3.52]], ... }
    #
    # The vertex labels used here should start from 1
    
    vertexMap = dict()
    adjacencyListMap = defaultdict(list)
    segments=[]

    for obstacle in polygons:
        for vertex in obstacle:
            if obstacle.index(vertex) == 0:
                prevV = obstacle[len(obstacle)-1]
                nextV = obstacle[1]
            elif obstacle.index(vertex) == len(obstacle) - 1:
                prevV = obstacle[len(obstacle)-2]
                nextV = obstacle[0]
            else:
                prevV=obstacle[obstacle.index(vertex)-1]
                nextV=obstacle[obstacle.index(vertex)+1]

                if vertex in reflexVertices and prevV in reflexVertices:
                    adjacencyListMap[reflexVertices.index(prevV)+1].append([reflexVertices.index(vertex)+1, (np.sqrt(((prevV[0]-vertex[0])**2)+((prevV[1]-vertex[1])**2)))])
                    adjacencyListMap[reflexVertices.index(vertex)+1].append([reflexVertices.index(prevV)+1, (np.sqrt(((vertex[0]-prevV[0])**2)+((vertex[1]-prevV[1])**2)))])    
                elif vertex in reflexVertices and nextV in reflexVertices:
                    adjacencyListMap[reflexVertices.index(vertex)+1].append([reflexVertices.index(nextV)+1, (np.sqrt(((vertex[0]-nextV[0])**2)+((vertex[1]-nextV[1])**2)))])
                    adjacencyListMap[reflexVertices.index(nextV)+1].append([reflexVertices.index(vertex), (np.sqrt(((nextV[0]-vertex[0])**2)+((nextV[1]-vertex[1])**2)))])
               
                segments.append([prevV, vertex])  
    i=1;
    for rVertex in reflexVertices:
        vertexMap[i]=rVertex  
        i+=1

        for sVertex in reflexVertices:
            if rVertex==sVertex:
                break
            else:
                for seg in segments:
                    if rVertex in seg or sVertex in seg:
                        break
                    else:
                        xdiff = (rVertex[0] - sVertex[0], seg[0][0] - seg[1][0])
                        ydiff = (rVertex[1] - sVertex[1], seg[0][1] - seg[1][1])
                        det = xdiff[0]*ydiff[1]-xdiff[1]*ydiff[0]
                        if (det==0):
                            adjacencyListMap[reflexVertices.index(rVertex)+1].append([reflexVertices.index(sVertex)+1, (np.sqrt(((rVertex[0]-sVertex[0])**2)+((rVertex[1]-sVertex[1])**2)))])
                            adjacencyListMap[reflexVertices.index(sVertex)+1].append([reflexVertices.index(rVertex)+1, (np.sqrt(((sVertex[0]-rVertex[0])**2)+((sVertex[1]-rVertex[1])**2)))])
               

        
    
  
    return vertexMap, adjacencyListMap

'''
Perform uniform cost search 
'''
def uniformCostSearch(adjListMap, start, goal):
    path = []
    pathLength = 0
    
    # Your code goes here. As the result, the function should
    # return a list of vertex labels, e.g.
    #
    # path = [23, 15, 9, ..., 37]
    #
    # in which 23 would be the label for the start and 37 the
    # label for the goal.

    return path, pathLength

'''
Agument roadmap to include start and goal
'''
def updateRoadmap(polygons, vertexMap, adjListMap, x1, y1, x2, y2):
    updatedALMap = dict()
    startLabel = 0
    goalLabel = -1

    # Your code goes here. Note that for convenience, we 
    # let start and goal have vertex labels 0 and -1,
    # respectively. Make sure you use these as your labels
    # for the start and goal vertices in the shortest path
    # roadmap. Note that what you do here is similar to
    # when you construct the roadmap. 

    return startLabel, goalLabel, updatedALMap


if __name__ == "__main__":
    # Retrive file name for input data
    if(len(sys.argv) < 0):
        print("Five arguments required: python spr.py [env-file] [x1] [y1] [x2] [y2]")
        exit()
    
    filename = "env_demo.txt"
    x1 = 1.0
    y1 = 2.0
    x2 = 3.0
    y2 = 4.0

    # Read data and parse polygons
    lines = [line.rstrip('\n') for line in open(filename)]
    polygons = []
    for line in range(0, len(lines)):
        xys = lines[line].split(';')
        polygon = []
        for p in range(0, len(xys)):
            polygon.append([float(i) for i in xys[p].split(',')])
        polygons.append(polygon)

    # Print out the data
    print("Pologonal obstacles:")
    for p in range(0, len(polygons)):
        print(str(polygons[p]))
    print("")

    # Compute reflex vertices
    reflexVertices = findReflexiveVertices(polygons)
    print("Reflexive vertices:")
    print(str(reflexVertices))
    print("")

    # Compute the roadmap 
    vertexMap, adjListMap = computeSPRoadmap(polygons, reflexVertices)
    print("Vertex map:")
    print(str(vertexMap))
    print("")
    print("Base roadmap:")
    print(dict(adjListMap))
    print("")

    # Update roadmap
    start, goal, updatedALMap = updateRoadmap(polygons, vertexMap, adjListMap, x1, y1, x2, y2)
    print("Updated roadmap:")
    print(dict(updatedALMap))
    print("")

    # Search for a solution     
    path, length = uniformCostSearch(updatedALMap, start, goal)
    print("Final path:")
    print(str(path))
    print("Final path length:" + str(length))
