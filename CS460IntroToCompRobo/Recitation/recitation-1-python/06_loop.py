'''
For loop and while loop
'''

# A basic for loop
# Print integers from 0 to 9
for i in range(10):
    print(i)

print()

# A basic while loop
# Print integers from 0 to 4
a = 0
while a < 5:
    print(a)
    a = a + 1

print()

# Continue and break
# Print integers from 0 to 9, exclude multiplications of 3
for i in range(10):
    if i % 3 == 0:
        continue
    print(i)

print()

# Print integers from 0 to 4
for i in range(10):
    if i > 5:
        break
    print(i)
