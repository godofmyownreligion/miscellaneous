'''
Logic operations
'''

# Boolean variables
a = True
b = False
print(a)
print(b)

# Logic operations: and, or, not
print(a and b)
print(a or b)
print(not a)

# The bool type is actually a number
print(1 + a)

# Other logic operations
print(5 == 3)
print(5 != 3)
print(5 >= 3)
