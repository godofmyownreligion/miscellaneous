"""
How to plot a histogram.
"""

import random
import matplotlib.pyplot as plt

# Gaussian distribution
data = [random.gauss(0, 10) for i in range(1000)]
plt.hist(data, bins=10, range=(-30, 30))
plt.xlabel('Value of Sample')
plt.ylabel('Number of Samples')
plt.title('This is a histogram')
plt.show()
