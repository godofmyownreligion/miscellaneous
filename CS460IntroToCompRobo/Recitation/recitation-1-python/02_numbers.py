'''
Basic variable: numbers.
'''

# Variable declaration and value assignment
anInteger = -1
print(anInteger)
anInteger = 1 + 1
print(anInteger)

# Floating point numbers
aFloat = 1.5
print(aFloat)

print()

# Basic numerical operations
print(anInteger + aFloat)
print(anInteger - aFloat)
print(anInteger * aFloat)
print(anInteger / aFloat)
print(5 / 3)
print(anInteger // aFloat)
