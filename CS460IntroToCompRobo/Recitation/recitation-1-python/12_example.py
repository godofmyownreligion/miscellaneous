'''
A simple python program which takes an input number and calculate 
    1 + 2 + 3 + ... + n
'''

n = int(input("Enter a number: "))
a = []
for i in range(n):
    print(i + 1, end="")
    if(i < n - 1):
        print(" + ", end = "")
    a.append(i + 1)
print(" =",sum(a))
 
print()
