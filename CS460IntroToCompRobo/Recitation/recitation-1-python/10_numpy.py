'''
numpy
'''

import numpy as np

# Numpy arrays
# pros: efficiency, functionality
# cons: flexibility
array_1 = np.array([[1, 2], [3, 4]])
array_2 = np.array([[5, 6], [7, 8]])
print(array_1.dot(array_2))
