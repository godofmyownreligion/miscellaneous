'''
Dictionary
'''

# Initialization
pronunciation = {1:"one"}
print(pronunciation)

# Get value according to key
print(pronunciation[1])

# Add key/value pair
pronunciation[35] = "thirty-fiv"
print(pronunciation)

# Modification
pronunciation[35] = "thirty-five"
print(pronunciation)
