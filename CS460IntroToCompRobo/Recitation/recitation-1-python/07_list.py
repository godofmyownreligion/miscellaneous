'''
List
'''

l = [1, 2, 3]
print(l)

# Iterate through all values in a list
for item in l:
    print(item)

# Access to a single value
print(l[0])

# Add an item
l.append(4)
print(l)
