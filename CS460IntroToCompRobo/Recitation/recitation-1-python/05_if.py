'''
The if statement
'''

# If
a = True
if a:
    print('This line is going to be printed out.')

a = False
if a:
    print('This line is not going to be printed out.')

# If-else
a = False
if a:
    print('This line is not going to be printed out.')
else:
    print('This line is going to be printed out.')

# If-elif-else
a = False
b = True
if False:
    print('This line is not going to be printed out.')
elif b:
    print('This line is going to be printed out.')
else:
    print('This line is not going to be printed out.')
