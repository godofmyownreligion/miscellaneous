'''
Function
'''

# Three essential components:
#     input
#     output
#     function name
def add(a, b):
    return a + b

print(add(3, 6))

print(add("mathe", "matics"))
