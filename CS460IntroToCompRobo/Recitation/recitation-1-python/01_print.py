"""
Use the print function to display stuff on the screen
"""

# print a number
print(1)

# print after calculation
print(1 + 1)

# print text
print('Hello world.')

# Basically everything in python is print-able
