CS 314 / Fall 2019 / Problem Set 6

1. Claim: Any language with a finite number of strings can be described by
a regular expression. Argue for or against.


2. Write context-free grammars that describe the same languages as these
regular expressions:

    - (abc)*
    - (a|b)*aa(a|b)*
    - b(a+b)*
    - a*b*


3. Consider this refinement to the NFA->DFA conversion method we discussed in
class: after finding the epsilon closure of a node, remove all non-accept
nodes that only have null transitions leading out.

Convert the regular expression a*a* to an NFA, and then convert that NFA
to a DFA using the original method and the revised method.


4. Convert the regular expression (a+b)* to an NFA, and then convert that NFA
to a DFA using the original method and the revised method.


5. Claim: this revised NFA->DFA method produces a DFA equivalent to the input
NFA. Argue for or against.
