:- use_module(library(clpfd)).

% tree(+Tree)  - succeeds when Tree is a binary tree
tree(Tree) :- false.

% symmetric(+Tree) - succeeds when Tree is a symmetric binary tree
symmetric(Tree) :- false.

% height(+Tree, ?Height) - succeeds when Tree is a binary tree with height
% Height
height(Tree, Height) :- false.

% perfect(?Tree, ?Height) - succeeds when Tree is a perfect binary tree with
% height Height
perfect(Tree, Height) :- false.

% sorted(+List) - succeeds when List is a list of integers in non-decreasing
% order
sorted(List) :- false.

% sselect(?Item, ?List, ?ListWithItem) - succeeds when List and ListWithItem
% are sorted lists of intgers, and ListWithItem is the result of inserting
% Item into List
sselect(Item, List1, List2) :- false.