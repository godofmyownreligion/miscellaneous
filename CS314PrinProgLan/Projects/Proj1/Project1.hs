module Project1 where

import Data.List
import Data.Foldable


triples :: [Integer] -> Integer
triples = fromIntegral . length . tripler


tripler :: [Integer] -> [Integer]
tripler [] = []
tripler xs = filter (\n -> n `mod` 3 == 0) xs


hailstone :: Integer -> [Integer]
hailstone 0 = []
hailstone 1 = [1]
hailstone start 
   |start `mod` 2 == 0 = start:hailstone (div start  2)
   |start `mod` 2 == 1 = start:hailstone (3 * start + 1)
   




data Point = Pt Double Double deriving (Show, Eq)

-- The centroid of a list of points is a point whose x (and y) coordinates are
-- the means of the x (and y) coordinates of the points in the list.
--
-- You may assume the list contains at least one point.
--
-- > centroid [Pt 1 1, Pt 2 2]
-- Pt 1.5 1.5
-- > centroid [Pt (-1.5) 0, Pt 3 2, Pt 0 1]
-- Pt 0.5 1.0

centroid :: [Point] -> Point
centroid = go 0 0 0
    where
    go l sx sy [] = Pt(sx/l)(sy/l)
    go l sx sy(Pt x y : ps)= go (l+1) (sx+s) (sy+y)ps




data Tree a = Tip | Bin (Tree a) a (Tree a) deriving (Show, Eq)



-- mirror returns a tree with the same shape and contents as its argument, but
-- flipped left for right.
--
-- > mirror (Bin (Bin Tip 1 (Bin Tip 2 Tip)) 3 Tip)
-- Bin Tip 3 (Bin (Bin Tip 2 Tip) 1 Tip)

mirror :: Tree a -> Tree a
mirror Tip = Tip
mirror (Bin l x r)= Bin (mirror r) x (mirror l)

-- In a strictly binary tree, each node has either 0 children or 2 children.
--
-- > strict (Bin Tip 1 Tip)
-- True
-- > strict (Bin Tip 1 (Bin Tip 2 Tip))
-- False

strict :: Tree a -> Bool
strict Tip = True
strict (Bin Tip _ Tip) = True
strict (Bin l@(Bin _ _ _) _ r@(Bin _ _ _))=strict l && strict r
strict _=False


-- A tree is near balanced if the left and right sub-trees of each node differ
-- in height by at most 1.
--
-- > near_balanced (Bin Tip 1 (Bin Tip 2 Tip))
-- True
-- > near_balanced (Bin Tip 1 (Bin Tip 2 (Bin Tip 3 Tip)))
-- False
-- > near_balanced (Bin (Bin Tip 2 Tip) 1 (Bin Tip 2 (Bin True 3 Tip)))
-- True
-- > near_balanced (Bin (Bin Tip 2 Tip) 1 (Bin Tip 2 (Bin (Bin Tip 4 Tip) 3 Tip)))
-- False

near_balanced :: Tree a -> Bool
near_balanced =  fst . nb
    where
    nb Tip = (True, 0)
    nb (Bin l _ r) = (bl && br && abs (hl - hr) < 2, 1 + max hl hr)
        where
        (bl, hl) = nb l
        (br, hr) = nb r

        