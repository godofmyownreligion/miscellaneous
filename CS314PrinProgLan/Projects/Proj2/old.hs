module Main where

import Control.Applicative
import Data.List
import System.Timeout
import Test.QuickCheck
import qualified Project2 as P

-- Properties

bounds :: [[Double]] -> Maybe (Int,Int)
bounds m
    | rows < 1            = Nothing
    | cols < 1            = Nothing
    | any (/= cols) colss = Nothing
    | otherwise           = Just (rows, cols)
    where
    rows = length m

    cols : colss = map length m


prop_zero_bounds (Dimension r) (Dimension c) = bounds (P.zero r c) == Just (r,c)

prop_zero_allzero (Dimension r) (Dimension c) = all (all (== 0)) (P.zero r c)

prop_ident_bounds (Dimension n) = bounds (P.ident n) == Just (n,n)

prop_ident_01 (Dimension n) = all (all (\x -> x == 0 || x == 1)) (P.ident n)

prop_diag_bounds (Matrix r c m) = length (P.diag m) == min r c

prop_add_bounds (MatrixPair r c a b) = bounds (P.add a b) == Just (r,c)

prop_add_commute (MatrixPair _ _ a b) = P.add a b == P.add b a

prop_add_neg (Matrix _ _ m) = all (all (== 0)) (P.add m (map (map negate) m) )

prop_add_zero (Matrix r c m) = P.add m (P.zero r c) == m

prop_transp_bounds (Matrix r c m) = bounds (P.transp m) == Just (c,r)

prop_transp_idem (Matrix _ _ m) = P.transp (P.transp m) == m

validSparse (P.Sparse r c m) = r > 0 && c > 0
    && increasingRange (-1) r (map fst m)
    && all (not . null . snd) m
    && all (increasingRange (-1) c . map fst . snd) m
    && all (all ((/= 0) . snd) . snd) m

increasingRange a z []     = True
increasingRange a z (x:xs) = a < x && x < z && increasingRange x z xs

negSparse (P.Sparse r c m) = P.Sparse r c (map (fmap (map (fmap negate))) m)

sparseLookup r c (P.Sparse _ _ m) = maybe 0 (maybe 0 id . lookup c) (lookup r m)

prop_sident_valid (Dimension n) = validSparse (P.sident n)

prop_sident_bounds (Dimension n) = r == n && c == n
    where P.Sparse r c _ = P.sident n

prop_sident_len (Dimension n) = length m == n
    where P.Sparse _ _ m = P.sident n

prop_sident_ones (Dimension n) = all (== 1) . map snd . concatMap snd $ m
    where P.Sparse _ _ m = P.sident n

prop_sdiag_bounds m@(P.Sparse r c _) = length (P.sdiag m) == min r c

prop_sdiag_diag m = and $ zipWith match [0..] (P.sdiag m)
    where
    match n x = sparseLookup n n m == x

prop_sadd_bounds (SparsePair x@(P.Sparse r c _) y) = r == r' && c == c'
    where P.Sparse r' c' _ = P.sadd x y

prop_sadd_valid (SparsePair x y) = validSparse (P.sadd x y)

prop_sadd_commute (SparsePair x y) = P.sadd x y == P.sadd y x

prop_sadd_neg m = null xs
    where P.Sparse _ _ xs = P.sadd m (negSparse m)

-- Reference implementations

zero :: Int -> Int -> [[Double]]
zero r c = replicate r (replicate c 0)


ident :: Int -> [[Double]]
ident 1 = [[1]]
ident n = (1 : replicate k 0) : map (0:) (ident k)
    where k = n - 1


diag :: [[Double]] -> [Double]
diag = diagFrom 0
    where
    diagFrom n [] = []
    diagFrom n (r:rs) =
        case drop n r of
            [] -> []
            x:_ -> x : diagFrom (n+1) rs


add :: [[Double]] -> [[Double]] -> [[Double]]
add = zipWith (zipWith (+))


transp :: [[Double]] -> [[Double]]
transp [r]    = map (:[]) r
transp (r:rs) = zipWith (:) r (transp rs)


sident :: Int -> P.Sparse
sident n = P.Sparse n n [(i,[(i,1)]) | i <- [0..n-1] ]


sdiag :: P.Sparse -> [Double]
sdiag (P.Sparse rows cols items) = make 0 items
    where
    len = min rows cols

    make n [] = replicate (len - n) 0
    make n _ | n >= len = []
    make n ((k,r):rs) =
        case compare n k of
            LT -> 0 : make (n+1) ((k,r):rs)
            EQ -> search n r : make (n+1) rs
            GT -> error "impossible"

    search n [] = 0
    search n ((k,d):cs) =
        case compare n k of
            LT -> 0
            EQ -> d
            GT -> search n cs


sadd :: P.Sparse -> P.Sparse -> P.Sparse
sadd (P.Sparse rows cols as) (P.Sparse _ _ bs) = P.Sparse rows cols (merge as bs)
    where
    merge :: [(Int,[(Int,Double)])] -> [(Int,[(Int,Double)])] -> [(Int,[(Int,Double)])]
    merge [] bs = bs
    merge as [] = as
    merge ((m,a):as) ((n,b):bs) =
        case compare m n of
            LT -> (m,a) : merge as ((n,b):bs)
            EQ  | null c    -> merge as bs
                | otherwise -> (m, mergeRows a b) : merge as bs
            GT -> (n,b) : merge ((m,a):as) bs
        where c = mergeRows a b

    screw = rows * cols

    mergeRows :: [(Int,Double)] -> [(Int,Double)] -> [(Int,Double)]
    mergeRows [] bs = bs
    mergeRows as [] = as
    mergeRows ((m,a):as) ((n,b):bs) =
        case compare m n of
            LT -> (m,a) : mergeRows as ((n,b):bs)
            EQ | c == 0    -> mergeRows as bs
               | otherwise -> (m, c) : mergeRows as bs
            GT -> (n,b) : mergeRows ((m,a):as) bs
        where c = a + b


-- Reference properties

prop_zero (Dimension r) (Dimension c) = zero r c == P.zero r c

prop_ident (Dimension n) = ident n == P.ident n

prop_diag (Matrix _ _ m) = diag m == P.diag m

prop_add (MatrixPair _ _ x y) = add x y == P.add x y

prop_transp (Matrix _ _ m) = transp m == P.transp m

prop_sident (Dimension n) = sident n == P.sident n

prop_sdiag m = sdiag m == P.sdiag m

prop_sadd (SparsePair x y) = sadd x y == P.sadd x y


-- Support code

newtype Dimension = Dimension Int deriving (Show)

instance Arbitrary Dimension where
    arbitrary = sized $ \n -> Dimension <$> choose (1,n+1)

    shrink (Dimension n) = [ Dimension m | m <- shrinkIntegral n, m > 0 ]


arbmatrix :: Int -> Int -> Gen [[Double]]
arbmatrix row col = vectorOf row (vector col)

drops :: [a] -> [[a]]
drops [] = []
drops (x:xs) = xs : map (x:) (drops xs)

shrinks :: (a -> [a]) -> [a] -> [[a]]
shrinks s [] = []
shrinks s (x:xs) = map (:xs) (s x) ++ map (x:) (shrinks s xs)

data Matrix = Matrix { rows :: Int, cols ::  Int, matr :: [[Double]] }

instance Show Matrix where
    showsPrec _ (Matrix r c (m:ms)) = showString "Matrix\n  { rows = "
        . shows r
        . showString "\n  , cols = "
        . shows c
        . showString "\n  , matr = ["
        . shows m
        . foldr (\m r -> showString ",\n            " . shows m . r) id ms
        . showString "]\n  }"

instance Arbitrary Matrix where
    arbitrary = do
        Dimension rows <- arbitrary
        Dimension cols <- arbitrary
        Matrix rows cols <$> arbmatrix rows cols

    shrink (Matrix r c m) = shrow ++ shcol ++ shelem
        where
        shrow
            | r > 1 = Matrix (r-1) c <$> drops m
            | otherwise  = []

        shcol
            | c > 1 = Matrix r (c-1) <$> mapM drops m
            | otherwise = []

        shelem = Matrix r c <$> shrinks (shrinks shrink) m

data MatrixPair = MatrixPair
    { prows :: Int
    , pcols :: Int
    , matrx :: [[Double]]
    , matry :: [[Double]]
    }

instance Show MatrixPair where
    showsPrec _ (MatrixPair r c (x:xs) (y:ys))
        = showString "Matrix\n  { prows = "
        . shows r
        . showString "\n  , pcols = "
        . shows c
        . showString "\n  , matrx = ["
        . shows x
        . foldr (\m r -> showString ",\n            " . shows m . r) id xs
        . showString "]\n  , matry = ["
        . shows y
        . foldr (\m r -> showString ",\n            " . shows m . r) id ys
        . showString "]\n  }"

instance Arbitrary MatrixPair where
    arbitrary = do
        Dimension rows <- arbitrary
        Dimension cols <- arbitrary
        MatrixPair rows cols <$> arbmatrix rows cols <*> arbmatrix rows cols

    shrink (MatrixPair r c x y) = shrow ++ shcol ++ shx ++ shy
        where
        shrow
            | r > 1     = MatrixPair (r-1) c <$> drops x <*> drops y
            | otherwise = []

        shcol
            | c > 1     = MatrixPair r (c-1) <$> mapM drops x <*> mapM drops y
            | otherwise = []

        shx = flip (MatrixPair r c) y <$> shrinks (shrinks shrink) x

        shy = MatrixPair r c x <$> shrinks (shrinks shrink) x

arbsparseOfNE g n k
    | n <= k    = (\k v -> [(k,v)]) <$> choose (0,n-1) <*> g
    | otherwise = oneof
        [ (\x xs -> (k,x):xs) <$> g <*> arbsparseOf g n (k+1)
        , arbsparseOfNE g n (k+1)
        ]

arbsparseOf :: Gen a -> Int -> Int -> Gen [(Int,a)]
arbsparseOf g n k
    | n <= k    = return []
    | otherwise = oneof
        [ (\x xs -> (k,x):xs) <$> g <*> arbsparseOf g n (k+1)
        , arbsparseOf g n (k+1)
        ]

arbsparse :: Int -> Int -> Gen [(Int,[(Int,Double)])]
arbsparse rows cols = arbsparseOf (arbsparseOfNE (getNonZero <$> arbitrary) cols 0) rows 0

shrinkNZ 0    = []
shrinkNZ 1    = []
shrinkNZ (-1) = []
shrinkNZ x    = signum x : [ signum x * (abs x ** (1 - 0.5^d)) | d <- [1..10]]

instance Arbitrary P.Sparse where
    arbitrary = do
        Dimension rows <- arbitrary
        Dimension cols <- arbitrary
        P.Sparse rows cols <$> arbsparse rows cols

    shrink (P.Sparse r c m)
        =  (P.Sparse r c <$> drops m)              -- clobber rows
        ++ (P.Sparse r c <$> shrinks shrinkRow m)  -- clobber items

        where
        shrinkRow (r,[(c,x)]) = [(r,[(c,y)]) | y <- shrinkNZ x]
        shrinkRow (r,cs) = map ((,) r) (drops cs ++ shrinks shrinkItem cs)

        shrinkItem (n,x) = map ((,) n) (shrinkNZ x)

data SparsePair = SparsePair P.Sparse P.Sparse deriving Show

instance Arbitrary SparsePair where
    arbitrary = do
        Dimension rows <- arbitrary
        Dimension cols <- arbitrary
        x <- arbsparse rows cols
        y <- arbsparse rows cols
        return $ SparsePair (P.Sparse rows cols x) (P.Sparse rows cols y)


-- Test cases

tests :: [(String, [(String, Property)], Property)]
tests =
    [ ("zero",
        [ ("bounds", property prop_zero_bounds)
        , ("allzero", property prop_zero_allzero)
        ],
        property prop_zero)
    , ("ident",
        [ ("bounds", property prop_ident_bounds)
        , ("all 0 or 1", property prop_ident_01)
        ],
        property prop_ident)
    , ("diag",
        [ ("bounds", property prop_diag_bounds)
        ],
        property prop_diag)
    , ("add",
        [ ("bounds", property prop_add_bounds)
        , ("x+y = y+x", property prop_add_commute)
        , ("x-x = 0", property prop_add_neg)
        , ("x+0 = x", property prop_add_zero)
        ],
        property prop_add)
    , ("transp",
        [ ("bounds", property prop_transp_bounds)
        , ("T(T(x)) = x", property prop_transp_idem)
        ],
        property prop_transp)
    , ("sident",
        [ ("valid", property prop_sident_valid)
        , ("bounds", property prop_sident_bounds)
        , ("nonzeros", property prop_sident_len)
        , ("ones", property prop_sident_ones)
        ],
        property prop_sident)
    , ("sdiag",
        [ ("bounds", property prop_sdiag_bounds)
        , ("correct", property prop_sdiag_diag)
        ],
        property prop_sdiag)
    , ("sadd",
        [ ("bounds", property prop_sadd_bounds)
        , ("valid", property prop_sadd_valid)
        , ("x-x=0", property prop_sadd_neg)
        , ("x+y=y+x", property prop_sadd_commute)
        ],
        property prop_sadd)
    ]

timelimit = 5000000

args = stdArgs { maxSuccess = 500 }

runprop :: String -> Property -> IO Double
runprop name prop = do
    putStrLn name
    r <- quickCheckWithResult args (within timelimit prop)
    case r of
        Success{} -> return 1
        _ -> return 0


runTestGroup (name, units, prop) = do
    putStrLn $ "\nTesting " ++ name
    unitpass <- fmap sum (mapM (uncurry runprop) units)
    proppass <- runprop "reference" prop
    return $ 5.0 * (unitpass / genericLength units + proppass)


runTests groups = do
    scores <- mapM runTestGroup groups
    putStrLn $ "Score: " ++ show (sum scores)


main = runTests tests
