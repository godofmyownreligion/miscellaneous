-- Revision 2

import Project3 (ABC(..), RE(..))
import qualified Project3 as P

import Control.Applicative
import Control.Exception
import Control.Monad
import Data.List (genericLength, nub)
import System.Timeout
import Test.QuickCheck


-- ensure correct types
atMost2As :: RE ABC
atMost2As = P.atMost2As

anyOf :: [a] -> RE a
anyOf = P.anyOf

repeats :: Int -> Int -> RE a -> RE a
repeats = P.repeats

matchEmpty :: RE a -> Bool
matchEmpty = P.matchEmpty

minLength :: RE a -> Integer
minLength = P.minLength

maxLength :: RE a -> Maybe Integer
maxLength = P.maxLength

firsts :: RE a -> [a]
firsts = P.firsts

-- reference implementation

ref_anyOf = foldr1 (:|:) . map Symbol

ref_repeats :: Int -> Int -> RE a -> RE a
ref_repeats m n r = foldr (:+:) optrs (replicate m r)
    where optrs = foldr (\x y -> Empty :|: (x :+: y)) Empty (replicate (n-m) r)

-- matchEmpty r indicates whether r will accept the empty string
ref_matchEmpty :: RE a -> Bool
ref_matchEmpty Empty = True
ref_matchEmpty (Symbol _) = False
ref_matchEmpty (a :|: b) = ref_matchEmpty a || ref_matchEmpty b
ref_matchEmpty (a :+: b) = ref_matchEmpty a && ref_matchEmpty b
ref_matchEmpty (Repeat r) = True
ref_matchEmpty (Plus r) = ref_matchEmpty r

-- minLength r returns the minimum length of any string that matches r
ref_minLength :: RE a -> Integer
ref_minLength Empty = 0
ref_minLength (Symbol _) = 1
ref_minLength (a :|: b) = min (ref_minLength a) (ref_minLength b)
ref_minLength (a :+: b) = ref_minLength a + ref_minLength b
ref_minLength (Repeat _) = 0
ref_minLength (Plus r) = ref_minLength r

-- maxLength r returns Maybe n if the maximum length of any string matched by
-- r is n, or Nothing if there is no upper limit
ref_maxLength :: RE a -> Maybe Integer
ref_maxLength Empty = Just 0
ref_maxLength (Symbol _) = Just 1
ref_maxLength (a :|: b) = liftM2 max (ref_maxLength a) (ref_maxLength b)
ref_maxLength (a :+: b) = liftM2 (+) (ref_maxLength a) (ref_maxLength b)
ref_maxLength (Repeat a) = if ref_maxLength a == Just 0 then Just 0 else Nothing
ref_maxLength (Plus a) = if ref_maxLength a == Just 0 then Just 0 else Nothing

-- firsts r returns a list containing every symbol that occurs first in some
-- string that matches r
ref_firsts :: RE a -> [a]
ref_firsts Empty = []
ref_firsts (Symbol a) = [a]
ref_firsts (a :|: b) = ref_firsts a ++ ref_firsts b
ref_firsts (a :+: b) = if ref_matchEmpty a then ref_firsts a ++ ref_firsts b else ref_firsts a
ref_firsts (Repeat a) = ref_firsts a
ref_firsts (Plus a) = ref_firsts a



-- creating arbitrary REs

arbRE :: Gen a -> Int -> Gen (RE a)
arbRE sym n
    | n < 1 = oneof [pure Empty, Symbol <$> sym]
    | otherwise = oneof
        [ pure Empty
        , Symbol <$> sym
        , (:+:) <$> arbRE sym k <*> arbRE sym k
        , (:|:) <$> arbRE sym k <*> arbRE sym k
        , Repeat <$> arbRE sym (n - 1)
        , Plus <$> arbRE sym (n - 1)
        ]
        where k = n `div` 2

instance Arbitrary a => Arbitrary (RE a) where
    arbitrary = sized (arbRE arbitrary)

    shrink Empty = []
    shrink (Symbol s) = Symbol <$> shrink s
    shrink (r :+: s) = [r,s] ++ [ r' :+: s | r' <- shrink r] ++ [ r :+: s' | s' <- shrink s ]
    shrink (r :|: s) = [r,s] ++ [ r' :|: s | r' <- shrink r] ++ [ r :|: s' | s' <- shrink s ]
    shrink (Repeat r) = r : (Repeat <$> shrink r)
    shrink (Plus r) = r : (Plus <$> shrink r)

instance Arbitrary ABC where
    arbitrary = oneof [pure A, pure B, pure C]

newtype Little = Little Int deriving (Show, Read, Eq, Ord)

instance Arbitrary Little where
    arbitrary = sized $ \n -> Little <$> choose (0, min n 10)
    shrink (Little n) = Little <$> shrink n


-- test cases
tests =
    [( "atMost2As",
        [ testRE atMost2As "AB" True
        , testRE atMost2As "AAB" True
        , testRE atMost2As "CB" True
        , testRE atMost2As "" True
        , testRE atMost2As "AAA" False
        , testRE atMost2As "ABBACCA" False
        ],
        [ prop "match" $ \str -> match atMost2As str == (length (filter (A==) str) < 3)
        ])
    , ( "anyOf",
        [ testRE oneof_bc "A" False
        , testRE oneof_bc "B" True
        , testRE oneof_bc "C" True
        , testRE oneof_bc "BC" False
        ],
        [ prop "minLength 1" $ \(NonEmpty syms) -> within timelimit $ ref_minLength (anyOf syms :: RE Int) == 1
        , prop "maxLength 1" $ \(NonEmpty syms) -> within timelimit $ ref_maxLength (anyOf syms :: RE Int) == Just 1
        , prop "firsts" $ \(NonEmpty syms) -> within timelimit $ ref_firsts (anyOf syms :: RE Int) `seteq` syms
        ])
    , ( "repeats",
        [ testRE repeats_bcs "" False
        , testRE repeats_bcs "BC" False
        , testRE repeats_bcs "BCBC" True
        , testRE repeats_bcs "BCBCBC" True
        , testRE repeats_bcs "BCBCBCBC" True
        , testRE repeats_bcs "BCBCBCBCBC" False
        , testRE (repeats 0 3 ac) "" True
        , testRE (repeats 0 3 ac) "ACACAC" True
        , testRE (repeats 0 3 ac) "ACACACAC" False
        , testRE (repeats 10 10 (Symbol A)) "AAAAAAAAAA" True
        ],
        [ prop "minLength" $ \(Little m) (Little n) r -> within timelimit $
            ref_minLength (repeats m (m+n) (r :: RE Int)) == fromIntegral m * ref_minLength r
        , prop "maxLength" $ \(Little m) (Little n) r -> within timelimit $
            ref_maxLength (repeats m (m+n) (r :: RE Int)) ==
                if m + n == 0 then Just 0 else fmap (fromIntegral (m+n) *) (ref_maxLength r)
        , prop "A" $ \(Little m) (Little n) s ->
            within timelimit $
                let b = match (repeats m (m+n) (Symbol A)) s
                in classify b "matching" $ b == match (ref_repeats m (m+n) (Symbol A)) s
        , prop "AC" $ \(Little m) (Little n) s ->
            within timelimit $
                let b = match (repeats m (m+n) ac) s
                in classify b "matching" $ b == match (ref_repeats m (m+n) ac) s
        , prop "A(B|C)" $ \(Little m) (Little n) s ->
            within timelimit $
                let b = match (repeats m (m+n) abc) s
                in classify b "matching" $ b == match (ref_repeats m (m+n) abc) s
        , prop "A*BC" $ \(Little m) (Little n) s ->
            within timelimit $
                let b = match (repeats m (m+n) asbc) s
                in classify b "matching" $ b == match (ref_repeats m (m+n) asbc) s
        ])
    , ( "matchEmpty",
        [ test "A" (matchEmpty (Symbol A)) False
        , test "A*" (matchEmpty (Repeat (Symbol A))) True
        , test "A+" (matchEmpty (Plus (Symbol A))) False
        , test "e" (matchEmpty Empty) True
        , test "e*" (matchEmpty (Repeat Empty)) True
        , test "A|e" (matchEmpty (opt (Symbol A))) True
        , test "(A|e)(B|e)" (matchEmpty (opt (Symbol A) :+: opt (Symbol B))) True
        , test "(A|e)B" (matchEmpty (opt (Symbol A) :+: Symbol B)) False
        , test "(A|e)+" (matchEmpty (Plus (opt (Symbol A)))) True
        , test "C(B|e)(A|e)" (matchEmpty (Symbol C :+: opt (Symbol B) :+: opt (Symbol A))) False
        , test "C(B|e)A" (matchEmpty (Symbol C :+: opt (Symbol B) :+: Symbol A)) False
        ],
        [ prop "reference" $ \r -> within timelimit $ matchEmpty (r :: RE ABC) == ref_matchEmpty r
        ])
    , ( "minLength",
        [ test "A" (minLength (Symbol A)) 1
        , test "A*" (minLength (Repeat (Symbol A))) 0
        , test "A+" (minLength (Plus (Symbol A))) 1
        , test "e" (minLength Empty) 0
        , test "e*" (minLength (Repeat Empty)) 0
        , test "A|e" (minLength (opt (Symbol A))) 0
        , test "(A|e)(B|e)" (minLength (opt (Symbol A) :+: opt (Symbol B))) 0
        , test "(A|e)B" (minLength (opt (Symbol A) :+: Symbol B)) 1
        , test "(A|e)+" (minLength (Plus (opt (Symbol A)))) 0
        , test "C(B|e)(A|e)" (minLength (Symbol C :+: opt (Symbol B) :+: opt (Symbol A))) 1
        , test "C(B|e)A" (minLength (Symbol C :+: opt (Symbol B) :+: Symbol A)) 2
        ],
        [ prop "reference" $ \r -> within timelimit $ minLength (r :: RE ABC) == ref_minLength r
        ])
    , ( "maxLength",
        [ test "A" (maxLength (Symbol A)) (Just 1)
        , test "A*" (maxLength (Repeat (Symbol A))) Nothing
        , test "A+" (maxLength (Plus (Symbol A))) Nothing
        , test "e" (maxLength Empty) (Just 0)
        , test "e*" (maxLength (Repeat Empty)) (Just 0)
        , test "A|e" (maxLength (opt (Symbol A))) (Just 1)
        , test "(A|e)(B|e)" (maxLength (opt (Symbol A) :+: opt (Symbol B))) (Just 2)
        , test "(A|e)B" (maxLength (opt (Symbol A) :+: Symbol B)) (Just 2)
        , test "(A|e)+" (maxLength (Plus (opt (Symbol A)))) Nothing
        , test "C(B|e)(A|e)" (maxLength (Symbol C :+: opt (Symbol B) :+: opt (Symbol A))) (Just 3)
        , test "C(B|e)A" (maxLength (Symbol C :+: opt (Symbol B) :+: Symbol A)) (Just 3)
        ],
        [ prop "reference" $ \r -> within timelimit $ maxLength (r :: RE ABC) == ref_maxLength r
        ])
    , ( "firsts",
        [ testBy seteq "A" (firsts (Symbol A)) [A]
        , testBy seteq "A*" (firsts (Repeat (Symbol A))) [A]
        , testBy seteq "A+" (firsts (Plus (Symbol A))) [A]
        , testBy seteq "e" (firsts Empty) ([] :: [ABC])
        , testBy seteq "e*" (firsts (Repeat Empty)) ([] :: [ABC])
        , testBy seteq "A|e" (firsts (opt (Symbol A))) [A]
        , testBy seteq "(A|e)(B|e)" (firsts (opt (Symbol A) :+: opt (Symbol B))) [A,B]
        , testBy seteq "(A|e)B" (firsts (opt (Symbol A) :+: Symbol B)) [A,B]
        , testBy seteq "(A|e)+" (firsts (Plus (opt (Symbol A)))) [A]
        , testBy seteq "C(B|e)(A|e)" (firsts (Symbol C :+: opt (Symbol B) :+: opt (Symbol A))) [C]
        , testBy seteq "C(B|e)A" (firsts (Symbol C :+: opt (Symbol B) :+: Symbol A)) [C]
        ],
        [ prop "reference" $ \r -> within timelimit $ firsts (r :: RE Int) `seteq` ref_firsts r
        ])
    ]

testRE r str = test str (match r (map (read . return) str))

oneof_bc = P.anyOf [B, C]
repeats_bcs = P.repeats 2 4 (Symbol B :+: Symbol C)

ac = Symbol A :+: Symbol C
abc = Symbol A :+: (Symbol B :|: Symbol C)
asbc = Repeat (Symbol A) :+: Symbol B :+: Symbol C

opt r = r :|: Empty

seteq s1 s2 = all (`elem` s1) s2 && all (`elem` s2) s1

interleave []     ys = ys
interleave (x:xs) ys = x : interleave ys xs

enumerate :: RE a -> [[a]]
enumerate Empty = [[]]
enumerate (Symbol x) = [[x]]
enumerate (r :|: s) = interleave (enumerate r) (enumerate s)
enumerate (r :+: s) = foldr (\x -> interleave (map (x ++) (enumerate s))) [] (enumerate r)
enumerate (Repeat r) = [] : enumerate (r :+: Repeat r)
enumerate (Plus r) = enumerate (r :+: Repeat r)

match :: (Eq a) => P.RE a -> [a] -> Bool
match r = any (null . snd) . matchPre r

matchPre :: (Eq a) => P.RE a -> [a] -> [(Bool,[a])]
matchPre (P.Symbol a) (x:xs) | x == a = [(True, xs)]
matchPre (P.Symbol a) _ = []
matchPre P.Empty xs = [(False, xs)]
matchPre (r P.:|: s) xs = matchPre r xs ++ matchPre s xs
matchPre (r P.:+: s) xs = do
    (r_consumed, ys) <- matchPre r xs
    (s_consumed, zs) <- matchPre s ys
    return (r_consumed || s_consumed, zs)
matchPre (P.Repeat r) xs = (False, xs) : do
    (r_consumed, ys) <- matchPre r xs
    guard r_consumed
    (_, zs) <- matchPre (P.Repeat r) ys
    return (True, zs)
matchPre (P.Plus r) xs = do
    (r_consumed, ys) <- matchPre r xs
    (s_consumed, zs) <- matchPre (P.Repeat r) ys
    return (r_consumed || s_consumed, zs)

timelimit = 10000000

testBy :: (Show a) => (a -> a -> Bool) -> String -> a -> a -> IO Bool
testBy eq name got want = catch body handle
    where
    body = do
        ok <- timeout timelimit (evaluate (eq got want))
        case ok of
            Just True -> return True
            Just False -> fail "FAIL " (show got)
            Nothing -> fail "TIMEOUT" "impatient"

    handle :: SomeException -> IO Bool
    handle e = fail "ERROR" (show e)

    fail msg txt = do
        putStrLn $ msg ++ ": " ++ name
        putStrLn $ "       wanted: " ++ show want
        putStrLn $ "       got:    " ++ txt
        return False

test :: (Eq a, Show a) => String -> a -> a -> IO Bool
test = testBy (==)

args = stdArgs { maxSuccess = 500 }

prop :: Testable prop => String -> prop -> IO Bool
prop name p = do
    putStrLn $ "Property: " ++ name
    r <- quickCheckWithResult args p
    case r of
        Success{} -> return True
        _ -> return False

testGroup :: (String, [IO Bool], [IO Bool]) -> IO Double
testGroup (name, units, props) = do
    putStrLn $ "\nTesting " ++ name
    unitpass <- fmap (genericLength . filter id) (sequence units)
    proppass <- fmap (genericLength . filter id) (sequence props)
    return $ 4 * unitpass / genericLength units + 6 * proppass / genericLength props

runTests groups = do
    putStrLn "Running unit tests for PA3 (release 2)."
    score <- fmap sum $ mapM testGroup groups
    putStrLn $ "\nScore: " ++ show score

main = runTests tests
