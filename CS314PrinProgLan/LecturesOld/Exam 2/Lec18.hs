-- These notes import a module from the "transformers" library. This is usually
-- included with GHC, but you may comment out or delete the last section and
-- remove the import of Control.Monad.Trans.State if your GHCi cannot find it.

module Lec18 where

import Control.Monad
import Control.Monad.Trans.State

data RE a            -- regular expression with alphabet a
    = Symbol a       -- match string [a]
    | Empty          -- match string []
    | RE a :|: RE a  -- choice (union)
    | RE a :+: RE a  -- concatenation
    | Repeat (RE a)  -- repeat zero or more times
    | Plus (RE a)    -- repeat one or more times
    deriving (Show, Read, Eq, Ord)


match :: (Eq a) => RE a -> [a] -> Bool
match r = any (null . snd) . matchPre r

matchPre :: (Eq a) => RE a -> [a] -> [(Bool,[a])]
matchPre (Symbol a) (x:xs) | x == a = [(True, xs)]
    -- if the first symbol matches a, we succeed and report that we consumed
    -- at least one symbol and have the remaining symbols as the left over

matchPre (Symbol a) _ = []
    -- if the first symbol is not a, or if there are no symbols, then fail

matchPre Empty xs = [(False, xs)]
    -- matching empty always succeeds by consuming no symbols with the entire
    -- string left over

matchPre (r :|: s) xs = matchPre r xs ++ matchPre s xs
    -- here we combine any the successes from matching r or s

matchPre (r :+: s) xs = do
    (r_consumed, ys) <- matchPre r xs
    (s_consumed, zs) <- matchPre s ys
    return (r_consumed || s_consumed, zs)

    -- for every (r_consumed, ys) obtained by matching r with xs,
    --    and for every (s_consumed, zs) obtained by matching s with ys,
    --        we consumed a symbol if r or s consumed and have zs left over

matchPre (Repeat r) xs = (False, xs) : do
    (r_consumed, ys) <- matchPre r xs
    guard r_consumed
    (_, zs) <- matchPre (Repeat r) ys
    return (True, zs)

    -- succeed by matching no symbols with the entire string left over, or
    -- for every (r_consumed, ys) obtained by matching r with xs,
    --    if r consumed anything,
    --        for every (_, ys) obtained by matching (Repeat r) with xs,
    --            succeed with zs left over, having consumed at least one symbol
    --
    -- We could rewrite this do block as a list comprehension:
    --
    -- [(True, zs) | (r_consumed, ys) <- matchPre r xs,
    --               r_consumed,
    --               (_, zs) <- matchPre (Repeat r) ys]
    --
    -- or we could use filter fst (matchPre r xs) to eliminate the cases where
    -- r does not consume input

matchPre (Plus r) xs = do
    (r_consumed, ys) <- matchPre r xs
    (s_consumed, zs) <- matchPre (Repeat r) ys
    return (r_consumed || s_consumed, zs)

    -- this is just matchPre (r :+: Repeat r)


-- Here are the regular expressions from Lec17.hs. You can confirm for yourself
-- that match will now match these regular expressions against any finite string
-- in finite time.

dangerous = Repeat (Symbol 'a' :|: Empty)
very_dangerous = Repeat (Repeat (Symbol 'a'))
okay = Repeat (Repeat (Symbol 'a') :+: Symbol 'b')
better = Empty :|: (Repeat (Symbol 'a' :|: Symbol 'b') :+: Symbol 'b')

-- However, we also see that the strategy matchPre uses can result in duplicated
-- work:
--
-- *Lec18> matchPre very_dangerous "aaab"
-- [(False,"aaab"),(True,"aab"),(True,"ab"),(True,"b"),(True,"b"),(True,"ab"),(True,"b"),(True,"b")]
--
-- Note that certain results occur multiple times in the list. If we were trying
-- to match very_dangerous as part of a larger regular expression, we may end
-- up testing whether the next part of the expression matches the input "b"
-- four times, even though we will get the same answer every time.
--
-- Again, it is possible to avoid this by filtering out duplicates, but this
-- runs in time n^2 * m, where n is the number of possible results and m is the
-- length of the suffix.
--
-- To obtain linear matching time, we will need a different representation.




-- Deterministic Finite Automata
-- -----------------------------

-- A Determinisitic Finite Automaton is defined by
-- 1. an alphabet
-- 2. a set of states
-- 3. an initial state
-- 4. a transition function that gives a state for every combination of state
--    and input symbol
-- 5. a set of accept states
--
-- We will parameterize our type by a (the type of symbol) and s (the type of
-- states). For the alphabet and set of states, we will use lists of symbols
-- and states, respectively. For the set of accept states, we will use a
-- function that returns True for accept states and False for any other state.

-- The definition given in class was
--
-- data DFA s a = DFA [a] [s] s (s -> a -> s) (s -> Bool)
--
-- However, working with this many fields is error-prone, so we can give these
-- fields labels by making DFA a "record".

data DFA s a = DFA
    { dalpha   :: [a]
    , dstates  :: [s]
    , dinit    :: s
    , dtransit :: s -> a -> s
    , daccept  :: s -> Bool
    }

-- We can treat DFA as a five-argument constructor just as before:

dfa2 :: DFA Int Int
dfa2 = DFA alpha states init transit accept
    where
    alpha  = [0,1]
    states = [0,1,2,-1]
    init   = 0

    transit 0 0 = 1
    transit 1 1 = 2
    transit 2 0 = 1
    transit _ _ = -1

    accept s = s > 0

-- Or we can use the record notation

dfa2r = DFA
    { dalpha   = [0,1]
    , dstates  = [0,1,2,-1]
    , dinit    = 0
    , dtransit = transit
    , daccept  = \s -> s > 0
    }
    where
    transit 0 0 = 1
    transit 1 1 = 2
    transit 2 0 = 1
    transit _ _ = -1

-- Additionally, we can use the field names to extract the fields from the
-- record.
--
-- *Lec18> dinit dfa2
-- 0
-- *Lec18> dstates dfa2
-- [0,1,2,-1]
-- *Lec18> map (daccept dfa2) (dstates dfa2)
-- [False,True,True,False]

runDFA :: DFA s a -> [a] -> Bool
runDFA dfa = daccept dfa . foldl (dtransit dfa) (dinit dfa)

-- *Lec18> runDFA dfa2 []
-- False
-- *Lec18> runDFA dfa2 [0]
-- True
-- *Lec18> runDFA dfa2 [1]
-- False
-- *Lec18> runDFA dfa2 [0,1]
-- True
-- *Lec18> runDFA dfa2 [0,1,1]
-- False
-- *Lec18> filter (runDFA dfa2) [[],[0],[1],[0,0],[0,1],[1,0],[1,1]]
-- [[0],[0,1]]






-- Extra bonus material: Using a monad to write matchPre
-- --------------------------------------

-- We did not have time to go over this in class, but we started using do to
-- write matchPre so that we could later switch to a different monad that would
-- allow us to write matchPre more conveniently.
--
-- Note that the type of matchPre ends with [a] -> [(Bool,[a])]. This is an
-- instance of a more general pattern: s -> m (b,s). This pattern is known as
-- a state transformer, because it potentially modifies a "state" represented
-- as a value of s.
--
-- The library "transformers" provides a type for this pattern: StateT s m a.
-- A value op :: StateT s m a can be thought of as an m-computation that
-- produces an a and has access to a mutable state containing s.
--
-- Control.Monad.Trans.State defines several functions for working with StateT,
-- including:
--
--    StateT    :: (s -> m (a,s)) -> StateT s m a
--    runStateT :: StateT s m a -> s -> m (a,s)
--
-- Using StateT, the type of matchPre becomes
--     (Eq a) => RE a -> StateT [a] [] Bool
-- indicating that it modifies a list of symbols while producing a Bool, and
-- that it may return zero or more answers.


matchPre2 :: (Eq a) => RE a -> StateT [a] [] Bool
matchPre2 (Symbol a) = StateT matchSymbol
    where
    matchSymbol (x:xs) | x == a = [(True, xs)]
    matchSymbol _ = []

    -- Matching a symbol is essentially the same code we had before, but now
    -- wrapped by StateT

matchPre2 Empty = return False
    -- Here, since we are not modifying the state (i.e., the unconsumed
    -- symbols), we only need to specify which Bool to produce

matchPre2 (r :|: s) = mplus (matchPre2 r) (matchPre2 s)
    -- As before, we combine the possible matches. mplus generalizes (++) to
    -- work with any monad that supports multiple answers.

matchPre2 (r :+: s) = do
    r_consumed <- matchPre2 r
    s_consumed <- matchPre2 s
    return (r_consumed || s_consumed)
    -- This is where using StateT makes life easier. The Monad instance for
    -- StateT manages the list of unconsumed symbols, rather than making us
    -- pass it around explicitly.
    --
    -- This pattern, where we perform two computations and then combine their
    -- results is common enough that Control.Monad provides a function that does
    -- it for us: liftM2 :: (Monad m) => (a -> b -> c) -> m a -> m b -> m c.
    -- Using it, we could have written this case as:
    --
    -- matchPre (r :+: s) = liftM2 (||) (matchPre r) (matchPre s)

matchPre2 (Repeat r) = mplus (return False) (do
    r_consumed <- matchPre2 r
    guard r_consumed
    matchPre2 (Repeat r)
    return True)

    -- As with the case for :|:, we use mplus to combine computations that
    -- produce different possible results. guard, like mplus, works for any
    -- monad that can produce multiple results, so we can use it here unchanged.

matchPre2 (Plus r) = liftM2 (||) (matchPre2 r) (matchPre2 (Repeat r))
    -- Again, this is the same as matchPre2 (r :+: Repeat r).


-- matchPre2 r returns a stateful list-computation, but we can use runStateT
-- to obtain the underlying function
--
-- *Lec18> :type matchPre2 very_dangerous
-- matchPre2 very_dangerous :: StateT [Char] [] Bool
--
-- *Lec18> :type runStateT (matchPre2 very_dangerous)
-- runStateT (matchPre2 very_dangerous) :: [Char] -> [(Bool, [Char])]
--
-- *Lec18> runStateT (matchPre2 very_dangerous) "aab"
-- [(False,"aab"),(True,"ab"),(True,"b"),(True,"b")]


match2 :: (Eq a) => RE a -> [a] -> Bool
match2 r = any (null . snd) . runStateT (matchPre2 r)
