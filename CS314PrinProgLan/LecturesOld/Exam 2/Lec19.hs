module Lec19 where

import Data.List

data DFA state symbol = DFA
    { alphabet :: [symbol]
    , states   :: [state]
    , initial  :: state
    , transit  :: state -> symbol -> state
    , accept   :: state -> Bool
    }

-- example from slides 3 and 5
example = DFA
    { alphabet = ['A','B']
    , states   = [1,2,3,4]
    , initial   = 1
    , transit  = delta
    , accept   = (/= 4)
    }
    where
    delta 1 'A' = 2
    delta 1 'B' = 1
    delta 2 'A' = 2
    delta 2 'B' = 3
    delta 3 'A' = 2
    delta 3 'B' = 4
    delta _  _  = 4

accepts :: DFA state symbol -> [symbol] -> Bool
accepts d = accept d . foldl (transit d) (initial d)

-- *Lec19> accepts example "AB"
-- True
-- *Lec19> accepts example "ABB"
-- False
-- *Lec19> accepts example "ABAB"
-- True
-- *Lec19> accepts example "BBBABAB"
-- True
-- *Lec19> accepts example "BBBABBAB"
-- False
-- *Lec19> accepts example ""
-- True


dfaUnion :: (Eq sym) => DFA s1 sym -> DFA s2 sym -> DFA (s1,s2) sym
dfaUnion d1 d2 = DFA
    { alphabet = intersect (alphabet d1) (alphabet d2)
        -- Formally, the union is only defined if d1 and d2 have the same
        -- alphabet, so we can make choices here.
        -- Another possibility would be to take the union of the alphabets.
        -- How would that be different
    , states   = [ (s1,s2) | s1 <- states d1, s2 <- states d2 ]
        -- Over-approximates the set of states: not all states in this set may
        -- be reachable
    , initial  = (initial d1, initial d2)
    , transit  = \(s1,s2) x -> (transit d1 s1 x, transit d2 s2 x)
        -- if d1 is in state s1, and d2 is in state s2, and the input is x,
        -- find the new states for d1 and d2
    , accept   = \(s1,s2) -> accept d1 s1 || accept d2 s2
    }

dfaIntersect :: (Eq sym) => DFA s1 sym -> DFA s2 sym -> DFA (s1,s2) sym
dfaIntersect d1 d2 = DFA
    { alphabet = intersect (alphabet d1) (alphabet d2)
    , states   = [ (s1,s2) | s1 <- states d1, s2 <- states d2 ]
    , initial  = (initial d1, initial d2)
    , transit  = \(s1,s2) x -> (transit d1 s1 x, transit d2 s2 x)
    , accept   = \(s1,s2) -> accept d1 s1 && accept d2 s2
    }


-- a dfa that accepts strings containing exactly one A
oneA = DFA
    { alphabet = ['A','B']
    , states   = [1,2,3]
    , initial  = 1
    , transit  = delta
    , accept   = (== 2)
    }
    where
    delta 1 'B' = 1
    delta 1 'A' = 2
    delta 2 'B' = 2
    delta _  _  = 3


-- *Lec19> accepts (dfaUnion example oneA) "AAA"
-- True
-- *Lec19> accepts (dfaUnion example oneA) "AAABB"
-- False
-- *Lec19> accepts (dfaUnion example oneA) "ABB"
-- True
-- *Lec19> accepts (dfaUnion example oneA) "BBABB"
-- True
-- *Lec19> accepts (dfaUnion example oneA) "BBABAB"
-- True
--
-- *Lec19> accepts (dfaIntersect example oneA) "AB"
-- True
-- *Lec19> accepts (dfaIntersect example oneA) "AAABB"
-- False
-- *Lec19> accepts (dfaIntersect example oneA) "ABB"
-- False
-- *Lec19> accepts (dfaIntersect example oneA) "BBABB"
-- False
-- *Lec19> accepts (dfaIntersect example oneA) "BBABAB"
-- False
-- *Lec19> accepts (dfaIntersect example oneA) "BBAB"
-- True



data NFA state symbol = NFA
    { nalphabet :: [symbol]
    , nstates   :: [state]
    , ninitial  :: state
    , ntransit  :: state -> Maybe symbol -> [state]
    , naccept   :: state -> Bool
    }

-- from slides 12 - 14
example2 :: NFA Char Char
example2 = NFA
    { nalphabet = ['a','b']
    , nstates   = ['X','Y','Z']
    , ninitial  = 'X'
    , ntransit  = delta
    , naccept   = (== 'Z')
    }
    where
    delta 'X' (Just 'a') = ['X', 'Y']
    delta 'X' (Just 'b') = ['X']
    delta 'Y' (Just 'b') = ['Z']
    delta 'Z' (Just 'a') = ['Z']
    delta 'Z' (Just 'b') = ['Z']
    delta _   _          = []

-- *Lec19> ntransit example2 'X' (Just 'a')
-- "XY"
-- *Lec19> map (\s -> ntransit example2 s (Just 'b')) (ntransit example2 'X' (Just 'a'))
-- ["X","Z"]
-- *Lec19> concatMap (\s -> ntransit example2 s (Just 'b')) (ntransit example2 'X' (Just 'a'))
-- "XZ"
-- *Lec19> map (\s -> ntransit example2 s (Just 'a')) (ntransit example2 'X' (Just 'a'))
-- ["XY",""]
-- *Lec19> concatMap (\s -> ntransit example2 s (Just 'a')) (ntransit example2 'X' (Just 'a'))
-- "XY"
-- *Lec19> ntransit example2 'X' (Just 'a') >>= \s -> ntransit example2 s (Just 'a')
-- "XY"

-- from the board: (A|BC)*
example3 :: NFA Int Char
example3 = NFA
    { nalphabet = ['A','B','C']
    , nstates   = [1,2,3,4,5,6,7,8]
    , ninitial  = 1
    , ntransit  = delta
    , naccept   = (== 8)
    }
    where
    delta 1 Nothing    = [2,4,8]
    delta 2 (Just 'A') = [3]
    delta 3 Nothing    = [8]
    delta 4 (Just 'B') = [5]
    delta 5 Nothing    = [6]
    delta 6 (Just 'C') = [7]
    delta 7 Nothing    = [8]
    delta 8 Nothing    = [1]
    delta _ _          = []


-- *Lec19> ntransit example3 1 Nothing
-- [2,4,8]
-- *Lec19> ntransit example3 1 (Just 'A')
-- []
-- *Lec19> ntransit example3 1 Nothing >>= \s -> ntransit example3 s Nothing
-- [1]
-- *Lec19> ntransit example3 1 Nothing >>= \s -> ntransit example3 s (Just 'A')
-- [3]

-- epsilonClosure nfa s0 returns every state we might be in after following zero
-- or more null transitions, starting from any state in s0
epsilonClosure :: (Eq state) => NFA state symbol -> [state] -> [state]
epsilonClosure nfa s0 = closure s0 s0
    where
    -- this helper function keeps track of which states have already been added
    -- to the closure, which avoids looping if the null transitions form a
    -- cycle; ss is the to-do list of states that have been seen but not yet
    -- examined
    closure seen []     = seen
    closure seen (s:ss) = closure (new ++ seen) (new ++ ss)
        where
        new = filter (\s -> not (elem s seen)) (ntransit nfa s Nothing)

-- *Lec19> ntransit example3 7 Nothing
-- [8]
-- *Lec19> ntransit example3 8 Nothing
-- [1]
-- *Lec19> ntransit example3 1 Nothing
-- [2,4,8]
-- *Lec19> epsilonClosure example3 [7]
-- [2,4,1,8,7]


-- nfaStep nfa s0 x returns every state we might reach after reading symbol x,
-- starting from any state in s0 and then following zero or more null
-- transitions
nfaStep :: (Eq state) => NFA state symbol -> [state] -> symbol -> [state]
nfaStep nfa s0 x = epsilonClosure nfa (foldr union [] dests)
    where
    dests = map (\s -> ntransit nfa s (Just x)) s0
    -- we use union instead of ++ to avoid testing the same transition more
    -- than once;


-- *Lec19> epsilonClosure example3 [1]
-- [2,4,8,1]
-- *Lec19> nfaStep example3 [1,2,4,8] 'A'
-- [2,4,1,8,3]
-- *Lec19> nfaStep example3 [1,2,3,4,8] 'B'
-- [6,5]
-- *Lec19> nfaStep example3 [6,5] 'C'
-- [2,4,1,8,7]


-- nfaAccepts nfa str returns whether str is part of the language of nfa
nfaAccepts :: (Eq state) => NFA state symbol -> [symbol] -> Bool
nfaAccepts nfa = any (naccept nfa) . foldl (nfaStep nfa) inits
    where inits = epsilonClosure nfa [ninitial nfa]

-- *Lec19> foldl (nfaStep example3) (epsilonClosure example3 [ninitial example3]) "BAC"
-- []
-- *Lec19> foldl (nfaStep example3) (epsilonClosure example3 [ninitial example3]) "ABC"
-- [2,4,1,8,7]
-- *Lec19> nfaAccepts example3 "ABC"
-- True
-- *Lec19> nfaAccepts example3 "ABCA"
-- True
-- *Lec19> nfaAccepts example3 "BCA"
-- True
-- *Lec19> nfaAccepts example3 "BAC"
-- False



example4 :: NFA Int Char
example4 = NFA
    { nalphabet = ['A', 'B']
    , nstates   = [1,2,3]
    , ninitial  = 1
    , ntransit  = delta
    , naccept   = \s -> s == 2 || s == 3
    }
    where
    delta 1 (Just 'A') = [2]
    delta 1 (Just 'B') = [3]
    delta 2 Nothing    = [1]
    delta _ _          = []


-- operations on NFAs

-- sym x is an NFA that accepts only the string [x]
sym :: (Eq sym) => sym -> NFA Bool sym
sym x = NFA
    { nalphabet = [x]
    , nstates   = [False, True]
    , ninitial  = False
    , ntransit  = delta
    , naccept   = id
    }
    where
    delta False (Just y) | x == y = [True]
    delta _ _ = []

-- *Lec19> nfaAccepts (sym 'a') "a"
-- True
-- *Lec19> nfaAccepts (sym 'a') "aa"
-- False
-- *Lec19> nfaAccepts (sym 'a') ""
-- False

empty :: NFA () sym
empty = NFA
    { nalphabet = []
    , nstates   = [()]
    , ninitial  = ()
    , ntransit  = \() _ -> []
    , naccept   = \() -> True
    }

-- *Lec19> nfaAccepts empty ""
-- True
-- *Lec19> nfaAccepts empty "a"
-- False


-- n1 .+. n2 will have all the states of both n1 and n2;
-- we will use Either to distinguish them
(.+.) :: (Eq sym) => NFA s1 sym -> NFA s2 sym -> NFA (Either s1 s2) sym
n1 .+. n2 = NFA
    { nalphabet = union (nalphabet n1) (nalphabet n2)
    , nstates   = map Left (nstates n1) ++ map Right (nstates n2)
    , ninitial  = Left (ninitial n1)
        -- concat n1 n2 starts with the initial state of n1
    , ntransit  = delta
    , naccept   = accept
    }
    where
    delta (Left s1) Nothing | naccept n1 s1
        = Right (ninitial n2) : map Left (ntransit n1 s1 Nothing)
        -- add a null transition from the accept states of n1 to the initial
        -- state of n2
    delta (Left s1) x = map Left (ntransit n1 s1 x)
    delta (Right s2) x = map Right (ntransit n2 s2 x)

    accept (Left _)   = False
    accept (Right s2) = naccept n2 s2
        -- the accept states of concat n1 n2 are the accept states of n2


-- *Lec19> nfaAccepts (sym 'a' .+. sym 'b') "ab"
-- True
-- *Lec19> nfaAccepts (sym 'a' .+. sym 'b') "abb"
-- False
-- *Lec19> nfaAccepts (sym 'a' .+. sym 'b') "bb"
-- False
-- *Lec19> nfaAccepts (sym 'a' .+. sym 'b') ""
-- False
-- *Lec19> nfaAccepts (sym 'a' .+. sym 'b') "a"
-- False
--
-- *Lec19> nstates (sym 'a' .+. sym 'b')
-- [Left False,Left True,Right False,Right True]
-- *Lec19> ninitial (sym 'a' .+. sym 'b')
-- Left False
-- *Lec19> let n = sym 'a' .+. sym 'b' in filter (naccept n) (nstates n)
-- [Right True]


-- n1 .|. n2 will have all the states of n1 and n2, and an additional state
(.|.) :: (Eq sym) => NFA s1 sym -> NFA s2 sym -> NFA (Maybe (Either s1 s2)) sym
n1 .|. n2 = NFA
    { nalphabet = union (nalphabet n1) (nalphabet n2)
    , nstates   = Nothing :
        (map (Just . Left) (nstates n1) ++ map (Just . Right) (nstates n2))
    , ninitial  = Nothing
    , ntransit  = delta
    , naccept   = accept
    }
    where
    delta Nothing Nothing
        = [Just (Left (ninitial n1)), Just (Right (ninitial n2))]
    delta Nothing _ = []
        -- from the new state, we can only take a null transition to the
        -- start states of n1 or n2
    delta (Just (Left s1))  x = map (Just . Left) (ntransit n1 s1 x)
    delta (Just (Right s2)) x = map (Just . Right) (ntransit n2 s2 x)

    accept (Just (Left s1))  = naccept n1 s1
    accept (Just (Right s2)) = naccept n2 s2
    accept Nothing           = False
        -- accept if we are in an accept state for n1 or n2

-- *Lec19> nfaAccepts (sym 'a' .|. sym 'b') "a"
-- True
-- *Lec19> nfaAccepts (sym 'a' .|. sym 'b') "b"
-- True
-- *Lec19> nfaAccepts (sym 'a' .|. sym 'b') ""
-- False
-- *Lec19> nfaAccepts (sym 'a' .|. sym 'b') "ab"
-- False
--
-- *Lec19> nstates (sym 'a' .|. sym 'b')
-- [Nothing,Just (Left False),Just (Left True),Just (Right False),Just (Right True)]
-- *Lec19> ninitial (sym 'a' .|. sym 'b')
-- Nothing
-- *Lec19> let n = sym 'a' .|. sym 'b' in filter (naccept n) (nstates n)
-- [Just (Left True),Just (Right True)]
--
-- *Lec19> let n = sym 'a' .|. sym 'b' in epsilonClosure n [ninitial n]
-- [Just (Left False),Just (Right False),Nothing]
-- *Lec19> let n = sym 'a' .|. sym 'b' in nfaStep n (epsilonClosure n [ninitial n]) 'a'
-- [Just (Left True)]

-- star n accepts any string comprising substrings accepted by n
-- star n has one more state than n
star :: (Eq state) => NFA state sym -> NFA (Maybe state) sym
star n = NFA
    { nalphabet = nalphabet n
    , nstates   = Nothing : map Just (nstates n)
    , ninitial  = Just (ninitial n)
    , ntransit  = delta
    , naccept   = accept
    }
    where
    delta Nothing  Nothing = [Just (ninitial n)]
        -- the final state has a null transition back to the start
    delta Nothing  _       = []
    delta (Just s) Nothing
        | naccept n s || s == ninitial n = Nothing : map Just (ntransit n s Nothing)
        -- the accept states in n have null transitions to the new accept state
        -- the initial state in n has a null transition to the new accept state
    delta (Just s) x       = map Just (ntransit n s x)

    accept Nothing = True
    accept _       = False

-- *Lec19> nfaAccepts (star (sym 'a')) ""
-- True
-- *Lec19> nfaAccepts (star (sym 'a')) "aaa"
-- True
-- *Lec19> nfaAccepts (star (sym 'a')) "aaab"
-- False
-- *Lec19> nfaAccepts (star (sym 'a' .+. sym 'b')) "aaab"
-- False
-- *Lec19> nfaAccepts (star (sym 'a' .+. sym 'b')) "ababab"
-- True
-- *Lec19> nfaAccepts (star (sym 'a' .+. sym 'b')) ""
-- True


-- plus n accepts any string comprising one or more substrings accepted by n
-- plus n has one more state than n
plus :: NFA state sym -> NFA (Maybe state) sym
plus n = NFA
    { nalphabet = nalphabet n
    , nstates   = Nothing : map Just (nstates n)
    , ninitial  = Just (ninitial n)
    , ntransit  = delta
    , naccept   = accept
    }
    where
    delta Nothing  Nothing = [Just (ninitial n)]
    delta Nothing  _       = []
    delta (Just s) Nothing | naccept n s = Nothing : map Just (ntransit n s Nothing)
        -- the accept states in n have null transitions to the new accept state
    delta (Just s) x       = map Just (ntransit n s x)

    accept Nothing = True
    accept _       = False

-- *Lec19> nfaAccepts (plus (sym 'a')) ""
-- False
-- *Lec19> nfaAccepts (plus (sym 'a')) "aaa"
-- True
-- *Lec19> nfaAccepts (plus (sym 'a')) "aaab"
-- False
-- *Lec19> nfaAccepts (plus (sym 'a' .+. sym 'b')) "aaab"
-- False
-- *Lec19> nfaAccepts (plus (sym 'a' .+. sym 'b')) "ababab"
-- True
-- *Lec19> nfaAccepts (plus (sym 'a' .+. sym 'b')) ""
-- False
