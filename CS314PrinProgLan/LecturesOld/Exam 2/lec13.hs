import Data.Ord
import Data.List

maxLength :: [[a]] -> Int
maxLength = maximum . map length

longest, shortest :: [[a]] -> [a]
longest = maximumBy (comparing length)

shortest = minimumBy (comparing length)



increasingLength, decreasingLength, decreasingLength' :: [[a]] -> [[a]]

increasingLength = sortBy (comparing length)

decreasingLength = sortBy (comparing (Down . length))

decreasingLength' = sortBy (flip (comparing length))
    -- equivalent to the earlier one




distance (x1,y1) (x2,y2) = sqrt ((x1-x2)^2 + (y1-y2)^2)

closestToOrigin, closestToOrigin' :: [(Double, Double)] -> (Double, Double)

closestToOrigin = minimumBy (comparing (distance (0,0)))

closestToOrigin' = minimumBy (comparing (distance (0,0))) . filter (/= (0,0))
    -- only return points that aren't the origin


permutationNumber
    :: Ord a => [a] -> [(Int,a)]
permutationNumber
    = map snd
    . sortBy (comparing fst)
    . zipWith (\i (x,y) -> (x,(i,y))) [0..]
    . sortBy (comparing snd)
    . zip [0..]
