{-# LANGUAGE RankNTypes #-}
-- This line informs GHC that we are using a Haskell extension that allows
-- for higher-ranked polymorphism. See the function nfaFromRE for details

module Lec20 where

import Lec19
import Prelude hiding (any, mapM)
import Control.Monad hiding (mapM)
import Control.Monad.Trans.State
import Data.Foldable (any)
import Data.Traversable (mapM)
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.IntMap as IM
import qualified Data.IntSet as IS

type Map    = Map.Map
type Set    = Set.Set
type IntMap = IM.IntMap
type IntSet = IS.IntSet

-- The containers library has included IS.disjoint since version 0.5.11, which
-- is more recent than the version on iLab. If working on the iLab,
-- remove this definition and uncomment the alternative one given below.
disjoint = IS.disjoint
-- disjoint a b = not $ IS.foldr (\x d -> IS.member x b || d) False a


data RE a            -- regular expression with alphabet a
    = Symbol a       -- match string [a]
    | Empty          -- match string []
    | RE a :|: RE a  -- choice (union)
    | RE a :+: RE a  -- concatenation
    | Repeat (RE a)  -- repeat zero or more times
    | Plus (RE a)    -- repeat one or more times
    deriving (Show, Read, Eq, Ord)

repeats 0 0 r = Empty
repeats 0 n r = (r :|: Empty) :+: repeats 0 (n-1) r
repeats m n r = r :+: repeats (m-1) (n-1) r


-- Part I: converting an RE to an NFA
--
-- In principle, the conversion function is trivially defined in terms of the
-- operators we defined in Lec19. For example, the regular expression
--     Repeat (Symbol A :|: Symbol B)
-- becomes
--     star (sym A .|. sym B)
--
-- What makes this difficult is the state type. We want to have something like
--      nfaFromRE :: RE a -> NFA state a
-- but what can we put for state? Leaving it polymorphic would mean that the
-- caller of nfaFromRE determines the state type, but actually the state type
-- is determined by the specific regular expression.
--
-- One possibility would be to rewrite all the operators to use some fixed
-- state type, such as Int. This would probably be the most efficient method,
-- especially if we avoid creating intermediate NFAs and just return the final
-- one immediately.
--
-- If we don't want to rewrite empty, sym, and the others, then we need to come
-- up with a way for nfaFromRE to avoid specifying its return type. A common
-- trick here is to pass the constructed value to another function (called the
-- continuation) and require that function to be polymorphic. That is, the
-- function should accept NFA state sym for all possible state types. Because
-- the continuation function is polymorphic, nfaFromRE is free to choose any
-- state type for the NFA it passes to the continuation.
--
-- Standard Haskell does not allow us to define such a function, but all
-- Haskell compilers for the past 20 years or so have supported extensions to
-- the language that enable this feature.

nfaFromRE :: Eq a => RE a -> (forall state. (Ord state) => NFA state a -> result) -> result
nfaFromRE (Symbol a) k = k (sym a)
nfaFromRE Empty      k = k empty
nfaFromRE (r :+: s)  k = nfaFromRE r (\a -> nfaFromRE s (\b -> k (a .+. b)))
    -- here, a is the NFA corresponding to r and b is the NFA corresponding to s
    -- a .+. b, which corresponds to r :+: s, is passed to the continuation k
nfaFromRE (r :|: s)  k = nfaFromRE r (\a -> nfaFromRE s (\b -> k (a .|. b)))
nfaFromRE (Repeat r) k = nfaFromRE r (\a -> k (star a))
nfaFromRE (Plus r)   k = nfaFromRE r (\a -> k (plus a))


-- One difficulty of this approach is that the continuation we provide to
-- nfaFromRE must be polymorphic over the state type, so its result must be
-- independent of the state type. Essentially, whatever we do with the NFA has
-- to be done inside the continuation.
--
-- For example:

tryNFA :: Eq state => NFA state Char -> IO ()
tryNFA nfa = do
    putStrLn "Enter a string"
    putStr "> "
    str <- getLine

    if nfaAccepts nfa str
        then putStrLn "Accepted"
        else putStrLn "Rejected"

    putStrLn "Another? (y/n)"
    putStr "> "
    ans <- getLine
    if take 1 ans == "y" then tryNFA nfa else return ()

-- *Lec20> nfaFromRE (Symbol 'a' :+: Repeat (Symbol 'b')) tryNFA
-- Enter a string
-- > abb
-- Accepted
-- Another? (y/n)
-- > y
-- Enter a string
-- > bba
-- Rejected
-- Another? (y/n)
-- > n


-- If we want to be able to work with the NFA outside a continuation, we will
-- need a way to hide its state type. Again, we can create an equivalent NFA
-- that uses Int as its state type.
intNFA :: (Ord state, Ord sym) => NFA state sym -> NFA Int sym
intNFA nfa = NFA
    { nalphabet = nalphabet nfa
    , nstates   = map snd statepairs
    , ninitial  = newstate (ninitial nfa)
    , ntransit  = \s x -> maybe [] id $ Map.lookup x =<< IM.lookup s transit
    , naccept   = \s -> IS.member s accepts
    }
    where

    statepairs = zip (nstates nfa) [0..]

    -- map from old state to int
    states = Map.fromList statepairs

    -- an unsafe lookup function that will fail if we call it with a non-state
    newstate s = case Map.lookup s states of
        Just n  -> n
        Nothing -> error "intNFA: reference to unknown state"

    -- the set of accept states, as an actual set
    accepts = IS.fromList
        . map snd
        . filter (naccept nfa . fst)
        $ statepairs
    -- an alternative would be to use a function that looks up the old state
    -- and then uses the old accept function; using a precomuted set of states
    -- gives us faster lookup times and does not require keeping the old
    -- function in memory

    -- the transition table, organized as a map from state to a map from
    -- symbol to list of states
    transit = IM.fromList $ map (\(s,n) -> (n, destTable s)) statepairs

    destTable s = Map.fromList
        . map (\(s,ds) -> (s, map newstate ds))
        . filter (not . null . snd)
        . map (\x -> (x, ntransit nfa s x))
        $ Nothing : map Just (nalphabet nfa)


ncompile :: (Ord sym) => RE sym -> NFA Int sym
ncompile re = nfaFromRE re intNFA

-- *Lec20> let n = ncompile (Symbol 'a' :+: Repeat (Symbol 'b'))
-- *Lec20> nalphabet n
-- "ab"
-- *Lec20> nstates n
-- [0,1,2,3,4]
-- *Lec20> filter (naccept n) (nstates n)
-- [2]
-- *Lec20> map (ntransit n 0) [Nothing, Just 'a', Just 'b']
-- [[],[1],[]]
-- *Lec20> map (ntransit n 1) [Nothing, Just 'a', Just 'b']
-- [[3],[],[]]
-- *Lec20> map (ntransit n 3) [Nothing, Just 'a', Just 'b']
-- [[2],[],[4]]
-- *Lec20> map (ntransit n 2) [Nothing, Just 'a', Just 'b']
-- [[3],[],[]]


-- Part II: converting an NFA to a DFA

-- Lec19 already contains the core parts of the NFA->DFA conversion algorithm:
-- epsilonClosure and nfaStep. Those use lists to represent sets of states,
-- but we will want to use an actual set type so that we can use them as keys in
-- a map. (Question: what problems might we have if we used lists?)


-- First, some generally useful functions.

-- setEpsilonClosure nfa init returns the epsilon closure of init in the NFA nfa
setEpsilonClosure :: (Ord state) => NFA state sym -> Set state -> Set state
setEpsilonClosure nfa init = visit init init
    where
    visit seen next
        | Set.null next = seen
        | otherwise = visit (Set.union new seen) new
            where
            new = Set.fromList
                . filter (\d -> not (Set.member d seen))
                . concatMap (\s -> ntransit nfa s Nothing)
                $ Set.toList next

-- setStep nfa init symbol returns every state that can be reached by following
-- a symbol transition from any state in init
setStep :: (Ord state) => NFA state sym -> Set state -> sym -> Set state
setStep nfa s0 x = setEpsilonClosure nfa
    . Set.fromList
    . concatMap (\s -> ntransit nfa s (Just x))
    $ Set.toList s0


-- dfaFromNFA nfa returns a DFA that accepts the same language as nfa
--
-- This is the primary conversion function. The bulk of the work happens in the
-- buildTable helper function, defined below, which repeatedly examines the
-- possible transitions for the sets of states the NFA might find itself in.
--
-- When reading the code, it is important to distinguish three kinds of state:
-- 1. The states in the NFA (indicated by values of type 'state')
-- 2. The states in the DFA (indicated by integer, and corresponding to sets of
--    NFA states)
-- 3. The state associated with the buildTable computation, comprising a list
--    of unexamined DFA states and a mapping from sets of NFA states to DFA
--    states. For convenience, buildTable and its helpers organize their code
--    using a monad to manage this computation state.
--
-- By convention, dfaFromNFA always uses 1 for the initial state and 0 for the
-- failure state.

dfaFromNFA :: (Ord state, Ord sym) => NFA state sym -> DFA Int sym
dfaFromNFA nfa = DFA
    { alphabet = nalphabet nfa
    , states   = 0 : IM.keys table
    , initial  = 1
    , transit  = \s x -> maybe 0 id $ Map.lookup x =<< IM.lookup s table
        -- lookup the transitions for starting state s
        -- if transitions are found, lookup the transition for symbol x
        -- if no transition is found, return the failure state
    , accept   = \s -> IS.member s accepts
    }
    where
    -- begin with the epsilon closure of the initial state
    inits = setEpsilonClosure nfa (Set.singleton (ninitial nfa))

    -- the initial NFA state set -> DFA state map
    -- the empty set maps to 0, and the initial set maps to 1
    initmap = Map.fromList [(Set.empty, 0), (inits, 1)]

    -- we evaluate the buildTable computation starting with the initial NFA
    -- state set as the to-do list, and the intial map
    --
    -- evalState executes the computation and discards its final state,
    -- returning just the list of triples it produces (see buildTable for
    -- details)
    tabledata = evalState (buildTable nfa) ([(1, inits)], initmap)

    -- using the table generated above, create a map from DFA states to
    -- maps from symbols to DFA states (Map Int (Map sym Int))
    table = IM.fromList $ map (\(n,d,_) -> (n,d)) tabledata
    accepts = IS.fromList $ map (\(n,_,_) -> n) (filter (\(_,_,b) -> b) tabledata)

-- *Lec20> let d = nfaFromRE (Repeat (Symbol 'a' :+: Symbol 'b')) dfaFromNFA
-- *Lec20> states d
-- [0,1,2,3]
-- *Lec20> filter (accept d) (states d)
-- [1,3]
-- *Lec20> map (transit d 1) "ab"
-- [2,0]
-- *Lec20> map (transit d 2) "ab"
-- [0,3]
-- *Lec20> map (transit d 3) "ab"
-- [2,0]
-- *Lec20> map (transit d 0) "ab"
-- [0,0]




-- shorthand: a computation Build state a produces a value of type a while
-- manipulating a to-do list and NFA state set to DFA state mapping, where
-- state is the type of the NFA state
type Build state a = State ([(Int, Set state)], Map (Set state) Int) a


-- buildTable nfa produces a list of triples containing
--    1. a DFA state
--    2. the transitions for that DFA state, represented as a Map
--    3. whether or not the DFA state is an accept state
--
-- buildTable manipulates a to-do list of unexamined NFA state sets and a
-- mapping from NFA state sets to DFA states. It continues until the to-do list
-- is empty.
buildTable :: (Ord sym, Ord state) => NFA state sym -> Build state [(Int, Map sym Int, Bool)]
buildTable nfa = do
    (to_do, dfaStates) <- get
    case to_do of
        [] -> return []   -- there is nothing do to, so return
        (n,s):nss -> do   -- get a DFA state n and its NFA state set s
            put (nss, dfaStates) -- update the to-do list
            d <- destTable nfa s -- get the transitions for this DFA state
            t <- buildTable nfa  -- get the rest of the table
            return ((n, d, any (naccept nfa) s) : t)

-- destTable nfa init produces a mapping from symbol to DFA state, representing
-- the transitions for the DFA state corresponding to init
--
-- destTable is structured as a sequence of transformations, so it may make the
-- most sense read from the bottom up
--
-- 1. start with the list of symbols
-- 2. for each symbol, find the set of states that would be reached by following
--    its transition
-- 3. remove any symbols with no transitions
-- 4. replace the set of states with the corresponding DFA state
-- 5. turn the list of symbol-DFA state pairs into a map
--
-- In step 4, we use stateSetId to look up the DFA state corresponding to the
-- set of NFA states. This returns computation that manipulates the buildTable
-- state, so we use mapM to combine the computations into a single computation
-- producing a list. Here, mapM effectively has the type
--     mapM :: (a -> Build state b) -> [a] -> Build state [b]
--
-- Step 5 uses fmap to change the list of pairs produced in step 4 into a Map.

destTable :: (Ord sym, Ord state)
    => NFA state sym -> Set state -> Build state (Map sym Int)
destTable nfa init
    = fmap Map.fromList
    . mapM (\(x,d) -> fmap ((,) x) (stateSetId d))
    . filter (not . Set.null . snd)
    . map (\x -> (x, setStep nfa init x))
    $ nalphabet nfa

-- stateSetId s returns a DFA state corresponding to s, a set of NFA states.
--
-- If s has been previously encountered, it returns the previously assigned DFA
-- state. If s has not been seen before, it assigns a fresh DFA state and adds
-- it to the to-do list and the NFA state set -> DFA state map.

stateSetId :: (Ord state) => Set state -> Build state Int
stateSetId s = state $ \(stack, states) ->
    case Map.lookup s states of
        Just i -> (i, (stack, states))
        Nothing -> let n = Map.size states in (n, ((n,s):stack, Map.insert s n states))



-- This is a variant of dfaFromNFA' that uses the refined NFA->DFA algorithm
-- discussed in problem set 7. The only change is the introduction of
-- nontrivial, the set of states that (a) have a non-null transition, or (b) are
-- accept states. Each time we call epsilonClosure or setStep, we filter the
-- set and keep only the nontrivial states.

dfaFromNFA' :: (Ord state, Ord sym) => NFA state sym -> DFA Int sym
dfaFromNFA' nfa = DFA
    { alphabet = nalphabet nfa
    , states   = 0 : IM.keys table
    , initial  = 1
    , transit  = \s x -> maybe 0 id $ Map.lookup x =<< IM.lookup s table
    , accept   = \s -> IS.member s accepts
    }
    where
    nontrivial = Set.fromList
        . filter (\s ->
               any (not . null . ntransit nfa s . Just) (nalphabet nfa)
            || naccept nfa s)
        $ nstates nfa

    inits = Set.intersection nontrivial
        $ setEpsilonClosure nfa (Set.singleton (ninitial nfa))

    initmap = Map.fromList [(Set.empty, 0), (inits, 1)]

    tabledata = evalState buildTable ([(1, inits)], initmap)

    table = IM.fromList $ map (\(n,d,_) -> (n,d)) tabledata

    accepts = IS.fromList $ map (\(n,_,_) -> n) (filter (\(_,_,b) -> b) tabledata)

    buildTable = do
        (st, sm) <- get
        case st of
            [] -> return []
            (n,s):nss -> do
                put (nss, sm)
                let accept = Set.foldr ((||) . naccept nfa) False s
                d <- destTable s
                fmap ((n,d,accept):) buildTable

    destTable init
        = fmap Map.fromList
        . mapM (\(x,d) -> fmap ((,) x) (stateSetId d))
        . filter (not . Set.null . snd)
        . map (\x -> (x, Set.intersection nontrivial (setStep nfa init x)))
        $ nalphabet nfa

-- *Lec20> let d = nfaFromRE (Repeat (Symbol 'a' :+: Symbol 'b')) dfaFromNFA'
-- *Lec20> states d
-- [0,1,2]
-- *Lec20> filter (accept d) (states d)
-- [1]
-- *Lec20> map (transit d 1) "ab"
-- [2,0]
-- *Lec20> map (transit d 2) "ab"
-- [0,1]
-- *Lec20> map (transit d 0) "ab"
-- [0,0]




-- Part III: the same ideas, expressed with different representations
--
-- In this part, we avoid using the NFA and DFA types we defined earlier, in
-- favor of NFATable and DFATable. These are alternative representations of
-- NFA's and DFA's that represent transitions with data structures instead of
-- functions.
--
-- In particular, this makes it easy to see all the transitions at once, without
-- needing to test every combination of state and input symbol.

-- For example:
dfaTableFromRE :: (Ord sym) => RE sym -> DFATable sym
dfaTableFromRE = dfaTableFromNFATable . nfaTableFromRE

-- *Lec20> dfaTableFromRE (Repeat (Symbol 'a' :+: Symbol 'b'))
-- DFATable
--     { dtRead = fromList [(1,fromList [('a',2)]),(2,fromList [('b',1)])]
--     , dtAcc = fromList [1]
--     }


-- For further efficiency, we use Haskell's IntSet and IntMap types wherever
-- possible. These use different representations internally that can be more
-- efficient in some cases. To do this, our NFA's and DFA's will label their
-- states with integers.
--
-- We will normally assume that the initial state is 0

data NFATable sym = NFATable
    { ntNull :: IntMap IntSet            -- the null transitions
    , ntRead :: IntMap (Map sym IntSet)  -- the symbol transitions
    , ntAcc  :: IntSet                   -- the accept states
    }
    deriving (Show)


nfaTableFromRE :: (Ord sym) => RE sym -> NFATable sym
nfaTableFromRE r = fst (makeNFATableFromRE r 0)

-- Lec20> nfaTableFromRE (Repeat (Symbol 'a' :+: Symbol 'b'))
-- NFATable
--     { ntNull = fromList
--         [(0,fromList [4])
--         ,(1,fromList [2])
--         ,(3,fromList [4])
--         ,(4,fromList [0])]
--     , ntRead = fromList
--         [(0,fromList [('a',fromList [1])])
--         ,(2,fromList [('b',fromList [3])])
--         ]
--     , ntAcc = fromList [4]
--     }


-- makeNFATableFromRE recursively generates an NFATable. To eliminate the need
-- to repeatedly relabel the NFA states, it ensures that the tables returned
-- in its recursive calls will have no overlap by specifying the initial state
-- label as an argument, and returning the next available label along with the
-- table. This allows us to combine transition maps using IM.union without
-- worrying that an entry with the same key might occur in both maps.
--
-- Note that makeNFATableFromRE r i returns a table with initial state i, not 0.

makeNFATableFromRE :: (Ord sym) => RE sym -> Int -> (NFATable sym, Int)
makeNFATableFromRE Empty i = (NFATable
    { ntNull = IM.empty
    , ntRead = IM.empty
    , ntAcc = IS.singleton i
    }, i + 1)
makeNFATableFromRE (Symbol x) i = (NFATable
    { ntNull = IM.empty
    , ntRead = IM.singleton i (Map.singleton x (IS.singleton j))
        -- transition from i to j when reading x
    , ntAcc  = IS.singleton j
    }, j + 1)
    where j = i + 1
makeNFATableFromRE (a :+: b) i = (NFATable
    { ntNull = IM.union nullA' nullB
    , ntRead = IM.union readA readB
    , ntAcc  = accB
    }, k)
    where
    (NFATable nullA readA accA, j) = makeNFATableFromRE a i
    (NFATable nullB readB accB, k) = makeNFATableFromRE b j
    nullA' = IS.foldr (addNullTo j) nullA accA
        -- add null transitions from every accept state in a to the start
        -- state of b
makeNFATableFromRE (a :|: b) i = (NFATable
    { ntNull = IM.insert i (IS.fromList [j,k]) (IM.union nullA nullB)
        -- add null transitions from the start state to the start states of
        -- a and b
    , ntRead = IM.union readA readB
    , ntAcc  = IS.union accA accB
    }, l)
    where
    j = i + 1
    (NFATable nullA readA accA, k) = makeNFATableFromRE a j
    (NFATable nullB readB accB, l) = makeNFATableFromRE b k
makeNFATableFromRE (Repeat a) i = (NFATable
    { ntNull = addNullTo i j
             . addNullTo j i
             $ IS.foldr (addNullTo j) nullA accA
        -- add null transitions from the accept states of a to the end state,
        -- from the start state to the end state, and from the end state to the
        -- start state
    , ntRead = readA
    , ntAcc  = IS.singleton j
    }, j + 1)
    where
    (NFATable nullA readA accA, j) = makeNFATableFromRE a i
makeNFATableFromRE (Plus a) i = (NFATable
    { ntNull = addNullTo i j $ IS.foldr (addNullTo j) nullA accA
        -- add null transitions from the accept states of a to the end state,
        -- and from the end state to the start state
    , ntRead = readA
    , ntAcc  = IS.singleton j
    }, j + 1)
    where
    (NFATable nullA readA accA, j) = makeNFATableFromRE a i


-- addNullTo j i nulls adds a null transition from i to j to the map nulls
-- note that the target is given first, since we frequently make multiple
-- null transitions to a given state
addNullTo
    :: Int  -- target of null transition
    -> Int  -- source of null transition
    -> IntMap IntSet -> IntMap IntSet
addNullTo t s = IM.insertWith IS.union s (IS.singleton t)


-- contrast with setEpsilonClosure
ntEpsilonClosure :: NFATable sym -> IntSet -> IntSet
ntEpsilonClosure nt init = eps init init
    where
    nullFrom i = maybe IS.empty id $ IM.lookup i (ntNull nt)
    eps seen next
        | IS.null next = seen
        | otherwise    = eps (IS.union seen new) new
        where
        new = IS.foldr (\i -> IS.union (IS.difference (nullFrom i) seen)) IS.empty next
            -- this effectively combines toList, concatMap, filter and fromList
            -- into a single fold over the elements of next

-- contrast with setStep
ntStep :: (Ord sym) => NFATable sym -> IntSet -> sym -> IntSet
ntStep nt ss x = IS.foldr (\s -> IS.union (dests s)) IS.empty ss
    where
    dests s = maybe IS.empty id $ Map.lookup x =<< IM.lookup s (ntRead nt)
    -- if the state has no transitions, or no transition for this symbol,
    -- return the empty set


-- DFAs in table form. Again, the convention is that the initial state is 1
-- and the failure state is 0.

data DFATable sym = DFATable
    { dtRead :: IntMap (Map sym Int)
    , dtAcc  :: IntSet
    }
    deriving Show

-- This is dfaFromNFA adapted to work with NFATable and DFATable
dfaTableFromNFATable :: (Ord sym) => NFATable sym -> DFATable sym
dfaTableFromNFATable nt@(NFATable _ read accept)
    = evalState buildTable ([(1, init)], Map.fromList [(IS.empty, 0), (init, 1)])
    where
    nontrivial = IS.union (IM.keysSet read) accept

    init = IS.intersection (ntEpsilonClosure nt (IS.singleton 0)) nontrivial

    buildTable = do
        (stack, dfaStates) <- get
        case stack of
            [] -> return (DFATable IM.empty IS.empty)
            (n,s):nss -> do
                put (nss, dfaStates)
                d <- destTable s
                DFATable read acc <- buildTable
                return $ DFATable
                    { dtRead = IM.insert n d read
                    , dtAcc  = if disjoint accept s then acc else IS.insert n acc
                        -- add n to the set of DFA accept states if s contains
                        -- any NFA accept states
                    }

    -- destTable init creates a DFA transition map corresponding to the set of
    -- NFA states in init
    --
    -- 1. look up all the transitions for every state in init, and combine them
    --    into a single transition map (symbol -> NFA state set)
    --
    -- 2. for each set of states:
    --     A. find its epsilon closure
    --     B. keep only the nontrivial states
    --     C. look up the corresponding DFA state
    --
    -- In step 2, we use mapM again, but on an IntMap instead of a list.
    -- It has the effective type:
    --    mapM :: (a -> BuildTable sym b) -> IntMap a -> BuildTable (IntMap b)
    -- where BuildTable a = State ([(Int,IntSet)], Map IntSet Int) a

    destTable = mapM (getId . IS.intersection nontrivial . ntEpsilonClosure nt)
              .  IS.foldr combine Map.empty

    combine i = Map.unionWith IS.union (maybe Map.empty id $ IM.lookup i read)

    -- IntSet -> Build Int
    getId s = state $ \(stack, dfaStates) ->
        case Map.lookup s dfaStates of
            Just i  -> (i, (stack, dfaStates))
            Nothing ->
                let n = Map.size dfaStates
                in (n, ((n,s):stack, Map.insert s n dfaStates))






dfaFromDFATable :: (Ord sym) => DFATable sym -> DFA Int sym
dfaFromDFATable (DFATable read acc) = DFA
    { alphabet = Set.toList $ IM.foldr (Set.union . Map.keysSet) Set.empty read
    , states   = 0 : IS.toList (IS.union acc (IM.keysSet read))
    , initial  = 1
    , transit  = \s x -> maybe 0 id $ Map.lookup x =<< IM.lookup s read
    , accept   = \s -> IS.member s acc
    }

compile :: (Ord sym) => RE sym -> DFA Int sym
compile = dfaFromDFATable . dfaTableFromRE

