module Lec17 where

data RE a            -- regular expression with alphabet a
    = Symbol a       -- match string [a]
    | Empty          -- match string []
    | RE a :|: RE a  -- choice (union)
    | RE a :+: RE a  -- concatenation
    | Repeat (RE a)  -- repeat zero or more times
    | Plus (RE a)    -- repeat one or more times
    deriving (Show, Read, Eq, Ord)


-- match1 r xs is true if xs is part of the language of r
match1 :: (Eq a) => RE a -> [a] -> Bool
match1 (Symbol a) [x] = x == a
match1 (Symbol a) xs  = False
    -- Symbol a matches only a 1-item string with the same symbol

match1 Empty      xs  = null xs
    -- Empty only matches the empty string

match1 (r :|: s)  xs  = match1 r xs || match1 s xs
    -- r :|: s matches any string that r matches, or that s matches

match1 (r :+: s)  xs  = any (\(p1,p2) -> match1 r p1 && match1 s p2) (splits xs)
    -- (r :+: s) matches xs if there is some way to split xs such that r matches
    -- the first part and s matches the second part

match1 (Repeat r) xs  = match1 (Empty :|: (r :+: Repeat r)) xs
    -- Repeat r matches the empty string and any string that matches
    -- r :+: Repeat r

match1 (Plus r)   xs  = match1 (r :+: Repeat r) xs
    -- Plus r matches any string that matches r :+: Repeat r


-- splits xs returns every way to split xs into two parts
splits [] = [([],[])]
splits (x:xs) = ([],x:xs) : map (\(p1, p2) -> (x:p1, p2)) (splits xs)



-- Improving performance with prefix matching
-- ------------------------------------------

-- match2 r xs is true when xs is part of the language of r
match2 :: (Eq a) => RE a -> [a] -> Bool
match2 r = any null . matchPre2 r
    -- if any of the suffixes returned by matchPre2 r xs are empty, that means
    -- r matched all of xs

-- matchPre2 r xs returns every possible suffix p2 such that xs == p1 ++ p2 and
-- p1 is part of the language of r
matchPre2 :: (Eq a) => RE a -> [a] -> [[a]]
matchPre2 (Symbol a) (x:xs) | a == x = [xs]
matchPre2 (Symbol a) xs = []
    -- Symbol a matches if a is the head of the string, leaving the tail left
    -- over, or does not match at all

matchPre2 Empty xs = [xs]
    -- Empty always succeeds, and the entire string is left over

matchPre2 (r :|: s) xs = matchPre2 r xs ++ matchPre2 s xs
    -- r :|: s succeeds if xs matches r or s; whatever is left over after
    -- matching r or s will be left over after matching r :|: s
    --
    -- Note: this may introduce duplicates; e.g., r :|: r will produce the same
    -- list of possible leftovers twice. We could avoid this by using set union
    -- instead of ++, but that introduces overhead proportional to the length
    -- of xs multiplied by the number of possible suffixes

matchPre2 (r :+: s) xs = do
    ys <- matchPre2 r xs
    zs <- matchPre2 s ys
    return zs
    -- for every suffix ys left over after matching r with xs,
    --    for every suffix zs left over after matching s with ys,
    --        zs will be left over after matching (r :+: s) with xs
    --
    -- This could also be written:
    --    matchPre2 r xs >>= \ys -> matchPre2 s ys >>= \zs -> return zs
    -- which is equivalent to
    --    matchPre2 r xs >>= \ys -> matchPre2 s ys >>= return
    -- which, by the monad laws, is equivalent to
    --    matchPre2 r xs >>= \ys -> matchPre2 s ys
    -- which is equivalent to
    --    matchPre2 r xs >>= matchPre2 s
    -- which, for lists, is equivalent to
    --    concat (map (matchPre2 s) (matchPre2 r xs))
    --
    -- *Lec17> matchPre2 (Repeat (Symbol 'a')) "aab"
    -- ["aab","ab","b"]
    -- *Lec17> map (matchPre2 (Symbol 'b')) (matchPre2 (Repeat (Symbol 'a')) "aab")
    -- [[],[],[""]]
    -- *Lec17> concat (map (matchPre2 (Symbol 'b')) (matchPre2 (Repeat (Symbol 'a')) "aab"))
    -- [""]
    -- *Lec17> matchPre2 (Repeat (Symbol 'a') :+: Symbol 'b') "aab"
    -- [""]

matchPre2 (Repeat r) xs = matchPre2 (Empty :|: (r :+: Repeat r)) xs
matchPre2 (Plus r) xs = matchPre2 (r :+: Repeat r) xs


-- match2 requires significantly less time to check for a match than match1,
-- entirely due to the improved performance in the r :+: s case.
--
-- *Match> match1 (Repeat (Symbol 'a') :+: Symbol 'b') (replicate 1000 'a')
-- False
-- (1.65 secs, 1,147,364,520 bytes)
-- *Match> match2 (Repeat (Symbol 'a') :+: Symbol 'b') (replicate 1000 'a')
-- False
-- (0.08 secs, 108,069,152 bytes)
--
-- However, neither match1 or match2 can handle the case where we repeat a
-- regular expression that can match the empty string, e.g.,

dangerous = Repeat (Symbol 'a' :|: Empty)

-- *Lec17> match1 dangerous ""
-- True
-- (0.01 secs, 76,456 bytes)
-- *Lec17> match1 dangerous "a"
-- ^CInterrupted.
-- *Lec17> match1 dangerous "b"
-- ^CInterrupted.
-- *Lec17> match2 dangerous ""
-- True
-- (0.00 secs, 76,296 bytes)
-- *Lec17> match2 dangerous "a"
-- True
-- (0.00 secs, 77,840 bytes)
-- *Lec17> match2 dangerous "b"
-- ^CInterrupted
--
-- Changing the way Repeat r is matched, by reversing the order to
-- (r :+: Repeat r) :+: Empty will improve some cases, while breaking others.
--
-- Similarly, reversing the list returned by splits will make match1 try the
-- longest possible match first, but this just moves the problem around.
--
-- Neither match1 or match2 can make any headway with this regular expression:

very_dangerous = Repeat (Repeat (Symbol 'a'))

-- *Lec17> match1 very_dangerous ""
-- True
-- (0.00 secs, 75,880 bytes)
-- *Lec17> match1 very_dangerous "a"
-- ^CInterrupted.
-- *Lec17> match2 very_dangerous ""
-- True
-- (0.00 secs, 76,296 bytes)
-- *Lec17> match2 very_dangerous "a"
-- ^CInterrupted.
--
-- Note that we do not have problems if the regular expression being repeated
-- does not match the empty string.

okay = Repeat (Repeat (Symbol 'a') :+: Symbol 'b')

-- *Lec17> match1 okay (replicate 100 'a')
-- False
-- (0.57 secs, 391,509,808 bytes)
-- *Lec17> match2 okay (replicate 100 'a')
-- False
-- (0.01 secs, 1,050,792 bytes)
--
-- Note the speed improvement of match2 compared to match1. This becomes more
-- dramatic as the input string grows longer.
--
-- *Lec17> match1 okay (replicate 200 'a')
-- False
-- (4.42 secs, 3,063,901,408 bytes)
-- *Lec17> match2 okay (replicate 200 'a')
-- False
--
-- It is also possible to rewrite the RE to match more quickly.

better = Empty :|: (Repeat (Symbol 'a' :|: Symbol 'b') :+: Symbol 'b')

-- *Lec17> match1 better (replicate 100 'a')
-- False
-- (0.03 secs, 13,455,432 bytes)
-- *Lec17> match2 better (replicate 100 'a')
-- False
-- (0.00 secs, 1,081,760 bytes)
--
-- Again, match2 shows better performance as the input string grows longer.
--
-- *Lec17> match1 better (replicate 500 'a')
-- False
-- (0.51 secs, 330,167,432 bytes)
-- *Lec17> match1 better (replicate 1000 'a')
-- False
-- (2.01 secs, 1,339,589,400 bytes)
-- *Lec17> match2 better (replicate 500 'a')
-- False
-- (0.03 secs, 24,026,080 bytes)
-- *Lec17> match2 better (replicate 1000 'a')
-- False
-- (0.08 secs, 108,389,616 bytes)
