-- this line is a module declaration
-- if you comment it out, Haskell will assume this module is named "Main"
module Lec8 where


-- Layout and Local definitions
-- ----------------------------

-- These definitions are equivalent. In average1, the semicolons and braces
-- are implied by the indentation structure.

average1 list =
    let
        total = sum list
        count = fromIntegral (length list)
    in total / count

average2 list =
    let {
        total = sum list;
        count = fromIntegral (length list);
    } in total / count;


-- length always returns an Int, but we want a fractional value (for example,
-- Float or Double), so we use fromIntegral to convert


-- We can use where to introduce local variables instead of let, if we like
-- that style better. Again, both versions are the same, but averagew1 leaves
-- the braces and semicolons implicit.

averagew1 list = total / count where
    total = sum list
    count = fromIntegral (length list)

averagew2 list = total / count where { total = sum list; count =
fromIntegral (length list)};

-- The where can be placed freely, as long as each definition is indented
-- (1) more than the definition overall, and (2) at least as much as the lines
-- before it


-- this is okay
averagew3 list = total / count
        where
    total = sum list
    count = fromIntegral (length list)


-- This won't parse correctly, because count is indented less than total
{-
averagew4 list = total / count
        where
        total = sum list
    count = fromIntegral (length list)
-}


-- A bunch of factorials
-- ---------------------

-- using if-then-else

fac1 n = if n > 1 then n * fac1 (n - 1) else 1

-- using case with n > 1

fac2 n =
    case n > 1 of
        False -> 1
        True  -> n * fac2 (n - 1)

-- using case with n

fac3 n =
    case n of
        0 -> 1
        _ -> n * fac3 (n - 1)

-- using multiple equations

fac4 0 = 1
fac4 n = n * fac4 (n - 1)

-- using a guarded equation

fac5 n | n <= 1 = 1
fac5 n          = n * fac5 (n - 1)

-- using multiple guards

fac6 n | n <= 1    = 1
       | otherwise = n * fac5 (n - 1)

    -- from the Prelude: otherwise = True
    -- this acts as a catch-all guard that always succeeds

-- note that fac3 and fac4 are unsafe if n is negative

-- Fizzbuzz
-- --------

fizzbuzz n
    | div3 && div5 = "fizzbuzz"
    | div3         = "fizz"
    | div5         = "buzz"
    | otherwise    = show n
    where
    div3 = mod n 3 == 0
    div5 = mod n 5 == 0

-- Lec8> fizzbuzz 1
-- "1"
-- Lec8> fizzbuzz 3
-- "fizz"
-- Lec8> fizzbuzz 5
-- "buzz"
-- Lec8> fizzbuzz 14
-- "14"
-- Lec8> fizzbuzz 15
-- "fizzbuzz"

-- Data types
-- ----------


data Point = Point Double Double
    -- this next line tells the compiler to extend "show" to handle this type
    deriving (Show)

-- Lec8> Point 1 1
-- Point 1.0 1.0

-- transpose_c and transpose are the same function, expressed using a case
-- statement and multiple equations, respectively

transpose_c pt =
    case pt of
        Point x y -> Point y x

transpose (Point x y) = Point y x

-- Lec8> transpose (Point 1 2)
-- Point 2.0 1.0



data MaybeInt = Yes Integer | No
    deriving (Show)

-- Lec8> Yes 5
-- Yes 5
-- Lec8> :type Yes 5
-- Yes 5 :: MaybeInt
-- Lec8> :type No
-- No :: MaybeInt

incMaybe_c x =
    case x of
        No    -> No
        Yes n -> Yes (n + 1)

incMaybe No      = No
incMaybe (Yes n) = Yes (n + 1)

unMaybe_c d x =
    case x of
        Yes n -> n
        No    -> d

unMaybe d No      = d
unMaybe d (Yes n) = n

-- Lec8> unMaybe 0 No
-- 0
-- Lec8> unMaybe 0 (Yes 1)
-- 1


-- When defining a function using multiple equations, they must all be grouped
-- together, without any other definitions in between (except local ones).
--
-- Thus, uncommenting this definition would be a compiler error

-- incMaybe (Yes 0) = No

-- Here is a function that uses MaybeInt to avoid division by 0:

safeDiv a 0 = No
safeDiv a b = Yes (div a b)

-- Note the following points:
-- 1. we use div for integer division, instead of /
-- 2. the two patterns overlap, so the equation that only matches 0 must be
-- given first; try switching the equations and entering the examples below

-- Lec8> safeDiv 4 2
-- Yes 2
-- Lec8> safeDiv 4 0
-- No
-- Lec8> incMaybe (safeDiv 4 2)
-- Yes 3
