module Lec9 where

data IntList = Nil | Cons Integer IntList deriving (Eq, Ord, Show, Read)

isEmpty Nil = True
isEmpty (Cons _ _) = False

listLen Nil = 0
listLen (Cons _ tail) = 1 + listLen tail

listSum Nil = 0
listSum (Cons x xs) = x + listSum xs

----

data Foo = Bar | Baz deriving (Eq, Ord, Read)

instance Show Foo where
    show Bar = "Bar"
    show Baz = "Baz"

----

-- An example of a custom numeric type. We will define a type for projective
-- integers, which are the integers extended with an infinite value.

data ProjectiveInt
    = Finite Integer
    | Infinite
    deriving (Eq, Ord, Show, Read)

-- note that the derived Ord instance uses the order of constructors,
-- so Finite n < Infinite and Finite m < Finite n if m < n

-- We can make ProjectiveInts behave like numbers by declaring a Num instance.
-- The implementations will re-use the implementations for Integer, but will
-- also handle Infinite values.

instance Num ProjectiveInt where
    Finite m + Finite n = Finite (m + n)
    _        + _        = Infinite
        -- infinity plus any value is infinity

    Finite m * Finite n = Finite (m * n)
    Finite 0 * Infinite = Finite 0
    Infinite * Finite 0 = Finite 0
    _        * _        = Infinite
        -- infinity times any non-zero value is infinity

    Finite m - Finite n = Finite (m - n)
    _        - _        = Infinite
        -- in projective numbers, infinity is both positive and negative, so
        -- x - infinity and infinity - x are both infinity

    -- negate will be defined using (-) if we don't provide our own definition

    abs (Finite m) = Finite (abs m)
    abs Infinite   = Infinite

    signum (Finite m) = Finite (signum m)
    signum Infinite   = Infinite
        -- four signs: zero, positive, negative, infinite

    fromInteger = Finite
        -- Finite :: Integer -> ProjectiveInt, which is the type needed for
        -- fromInteger, so we can just use it as the definition

-- defining an instance of Num allows us to use numeric literals
--
-- Lec9> 1 + 2 :: ProjectiveInt
-- Finite 3
-- Lec9> 1 + Infinite
-- Infinite
-- Lec9> Infinite * 0
-- Finite 0

-- we can't sensibly define an instance of Integral, so we will provide our own
-- division function

pdiv _          (Finite 0) = Infinite
pdiv (Finite m) (Finite n) = Finite (div m n)
pdiv Infinite   _          = Infinite
pdiv _          Infinite   = 0

-- Lec9> pdiv 1 0 :: ProjectiveInt
-- Infinite
-- Lec9> pdiv 1 Infinite
-- Finite 0
