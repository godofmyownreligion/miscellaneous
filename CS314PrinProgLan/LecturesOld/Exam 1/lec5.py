# Note: this code requires Python 3
#
# to test, enter "python3 lec5.py" on your command line

def make_count():
    X = 0

    def count():
        nonlocal X   # tell python we want the parent's X, not a new X
        X = X + 1
        return X

    def reset():
        nonlocal X
        X = 0

    return (count, reset)


# only execute this code when running directly from the command line
if __name__ == '__main__':

    # make two independent counters
    (c1,r1) = make_count()
    (c2,r2) = make_count()

    print(c1()) # prints 1
    print(c1()) # prints 2
    print(c2()) # prints 1
    r1()        # resets c1 to 0
    print(c1()) # prints 1
