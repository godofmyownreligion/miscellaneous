:- use_module(library(clpfd)).

bad_mergesort([], []).
bad_mergesort([X], [X]).
bad_mergesort([X,Y|Unsorted], Sorted) :-
    partition(Unsorted, L1, L2),
    bad_mergesort([X|L1], SL1),
    bad_mergesort([Y|L2], SL2),
    bad_merge(SL1, SL2, Sorted).


bad_merge([], Ys, Ys).
bad_merge([X|Xs], [], [X|Xs]).
bad_merge([X|Xs], [Y|Ys], Zs) :- zcompare(C,X,Y), bad_merge(C,X,Xs,Y,Ys,Zs).

bad_merge(<, X, Xs, Y, Ys, [X|Zs]) :- bad_merge(Xs, [Y|Ys], Zs).
bad_merge(=, X, Xs, Y, Ys, [X|Zs]) :- bad_merge(Xs, [Y|Ys], Zs).
bad_merge(>, X, Xs, Y, Ys, [Y|Zs]) :- bad_merge([X|Xs], Ys, Zs).





mergesort([], []).
mergesort([X|Xs], Sorted) :- mergesort(Xs, X, Sorted).

mergesort([], X, [X]).
mergesort([Y|Unsorted], X, Sorted) :-
    partition(Unsorted, L1, L2),
    mergesort(L1, X, SL1),
    mergesort(L2, Y, SL2),
    merge(SL1, SL2, Sorted).

partition([], [], []).
partition([X|Xs], [X|Ys], Zs) :-
	partition(Xs, Zs, Ys).


merge([], Ys, Ys).
merge([X|Xs], Ys, Zs) :- merge(Ys, X, Xs, Zs).

merge([], X, Xs, [X|Xs]).
merge([Y|Ys], X, Xs, Zs) :- 
	X #=< Y #<==> B,
	merge(B,X,Xs,Y,Ys,Zs).

merge(1,X,Xs,Y,Ys,[X|Zs]) :- merge(Xs,Y,Ys,Zs).
merge(0,X,Xs,Y,Ys,[Y|Zs]) :- merge(Ys,X,Xs,Zs).




my_once(G) :- call(G), !.    % danger!


% -----

% permutation(+List, -PermutedList)
permutation([], []).
permutation(Xs, [E|Es]) :-
    select(E, Xs, Ys),
    permutation(Ys, Es).

ordered([]).
ordered([E|Es]) :- ordered(Es, E).

ordered([], _).
ordered([E|Es], X) :- X #=< E, ordered(Es, E).


slowsort(List, Sorted) :-
    permutation(List, Sorted),
    ordered(Sorted).

slowsort2(List, Sorted) :-
    ordered(Sorted),
    permutation(List, Sorted).


same_length([], []).
same_length([_|A], [_|B]) :- same_length(A, B).

slowsort3(List, Sorted) :-
    same_length(List, Sorted),
    ordered(Sorted),
    permutation(List, Sorted).

% How are slowsort, slowsort2, and slowsort3 different?