module Parser where

import Control.Applicative
import Control.Monad
import Data.List


-- We will build parsers out of a few simple primitives. A value of type
-- "Parser a" describes what strings to accept and will produce a value of
-- type "a" if it succeeds.
--
-- We can use parse :: Parser a -> String -> Maybe a to test (1) whether a
-- string will parse correctly, and (2) to obtain the value produced by a
-- successful parse.
--
-- For example, ch '0' succeeds for the string "0".
--
-- REP> parse (ch '0') "0"
-- Just '0'
--
-- We can use <|> to represent a choice between two parsers:

bitChar = ch '0' <|> ch '1'

-- REP> parse bitChar "0"
-- Just '0'
-- REP> parse bitChar "1"
-- Just '1'
--
-- We can use <$> to apply a function to the result of a successful parse.

bit = (== '1') <$> bitChar

-- REP> parse bit "0"
-- Just False
-- REP> parse bit "1"
-- Just True

-- We can use <*> or liftA2 to combine two parsers.
--
--    liftA2 :: (a -> b -> c) -> Parser a -> Parser b -> Parser c
--    (<*>) :: Parser (a -> b) -> Parser a -> Parser b
--
-- This is simplest to see with liftA2 (,) p q, which will match strings
-- that can be divided into two parts such that the first part matches p and
-- the second matches q. The result will be a pair containing the values
-- produced by p and q.
--
-- REP> parse (liftA2 (,) bit bit) "01"
-- Just (False, True)
--
-- In general, liftA2 f p q will apply f to the values produced by p and q and
-- produce the result of f.
--
-- REP> parse (liftA2 (&&) bit bit) "01"
-- Just False
--
-- Note that liftA2 f p q is equivalent to f <$> p <*> q. Conversely,
-- p <*> q is equivalent to liftA2 ($) p q.
--
-- Note also that f <$> p is equivalent to pure f <*> p.

-- We can use two built-in functions to match a string multiple times.
--
--   many :: Parser a -> Parser [a]  -- matches zero or more times
--   some :: Parser a -> Parser [a]  -- matches one or more times
--
-- REP> parse (many bit) ""
-- Just []
-- REP> parse (many bit) "0110"
-- Just [False,True,True,False]
-- REP> parse (some bit) ""
-- Nothing
--
-- some and many are convenient, but we can also use recursion:

bits = (:) <$> bit <*> (bits <|> pure [])

-- Breaking this down, bit will consume a single character and produce a Bool.
-- Next, we have have a choice between using bits to consume multiple characters
-- to produce a [Bool], or using pure [] to consume no characters and produce
-- a [Bool]. Finally, we use (:) to combine the Bool and [Bool] into a single
-- [Bool].
--
-- Note that bits requires the input to contain at least one character, while
-- bits <|> pure [] will also succeed for an empty input (because pure [] always
-- succeeds).

-- We define oneof :: [Char] -> Parser Char to match any of the provided
-- characters and return the character it received.
--
-- REP> parse (oneof "abc") "b"
-- Just 'b'

digit = digChr <$> oneof ['0' .. '9']

digChr x = fromIntegral (fromEnum x - fromEnum '0')

-- REP> digChr '8'
-- 8
-- REP> parse digit "6"
-- Just 6

-- Now, using some digit or by writing the recursion ourself, we can get a list
-- of digits and then use foldl' to turn this into an Integer.

int = foldl' (\a d -> a * 10 + d) 0 <$> some digit

-- REP> foldl' (\a d -> a * 10 + d) 0 [1,2,3]
-- 123
-- REP> parse int "123"
-- Just 123



-- Regular Expressions
-- -------------------

-- This is the regular expression abstract syntax tree we have been using.

data RE a
    = Never
    | Empty
    | Symbol a
    | RE a :+ RE a
    | RE a :* RE a
    | Repeat (RE a)
    | Plus (RE a)
    deriving (Show, Eq)


-- The usual notation for regular expressions has the follwing grammar:
--
-- re ::= choice | re
-- re ::= choice
--
-- choice ::= part choice
-- choice ::= part
--
-- part ::= part *
-- part ::= part +
-- part ::= atom
--
-- atom ::= ( re )
-- atom ::= e
-- atom ::= symbol
--
-- To use this with Parser, we can define a parser for each non-terminal.
-- We must also rework part to eliminate left recursion.
--
-- part ::= atom stars
--
-- stars ::= * stars
-- stars ::= + stars
-- stars ::=

-- We will define a Parser for each non-terminal. The main ones will produce
-- a RE Char. We use <|> to combine multiple rules, and we will use <*> to
-- combine multiple symbols in a single rule.

-- re ::= choice | re
-- re ::= choice

re = (:+) <$> choice <* ch '|' <*> re
   <|> choice

-- The first rule has three symbols: choice, |, and re. Of these, we will want
-- to combine the values produced by choice and re to get the final RE Char.
-- Here, we chose to use <* to discard the Char produced by ch '|'. Another
-- approach would be to have the combining function discard the Char.
--
--      (\l b r -> l :+ r) <$> choice <*> ch '|' <*> re
--
-- This definition does have an efficiency problem. If our string matches choice
-- without going on to |, we will need to backtrack to the most recent <|>,
-- at which point we try to parse choice again. Later on, we will show how to
-- refactor the grammar to avoid this duplicated effort.

-- choice ::= part choice
-- choice ::= part

choice = (:*) <$> part <*> choice
    <|> part

-- atom ::= ( re )
-- atom ::= e
-- atom ::= Symbol

atom = ch '(' *> re <* ch ')'
    <|> ch 'e' *> pure Empty
    <|> Symbol <$> oneof (['A'..'Z'] ++ "abcd" ++ ['f'..'z'])

-- For atom, we had to make a choice about what symbols are. We chose every
-- letter except lower-case e, which we are using for epsilon. Note as well
-- that we make sure to return values of RE Char that correspond to what we
-- have parsed.

-- Now for the tricky one. * and + are post-fix operators, which means that we
-- get the expression that goes inside Repeat before we know whether we want
-- to use Repeat.
--
-- part ::= atom stars
--
-- stars ::= * stars
-- stars ::= + stars
-- stars ::=
--
-- We can take a few approaches here. One is to say that part is an atom
-- followed by zero or more stars, so we could have stars return a list of
-- constructors to apply to the RE Char produced by atom.

star = ch '*' *> pure Repeat
    <|> ch '+' *> pure Plus

part = foldl' (flip ($)) <$> atom <*> many star

-- REP> :type many star
-- many star :: Parser [RE a -> RE a]
-- REP> :type (flip ($))
-- flip ($) :: a -> (a -> b) -> b
-- REP> :type foldl' (flip ($))
-- foldl' (flip ($)) :: a -> [a -> a] -> a
--
-- You can confirm the following identities yourself:
--
--   foldl (flip ($)) p [] = p
--
--   foldl (flip ($)) p (f:fs) = foldl (flip ($)) (flip ($) p f) fs
--                             = foldl (flip ($)) (f p) fs

-- As with bits, we can define stars directly without using many. Instead of
-- producing a list of functions, stars will produce a single function.
-- We can use <**> to apply the function produced by stars to the value produced
-- by atom.

part' = atom <**> stars'

stars' = flip (.) <$> star <*> stars'
    <|> pure id

-- We use flip (.) :: (a -> b) -> (b -> c) -> (a -> c) to combine the functions
-- produced by star and stars', such that the function produced by star will be
-- applied first.
--
-- REP> parse part' "a*+"
-- Just (Plus (Repeat (Symbol 'a')))
--
-- Try replacing flip (.) with (.) to see how this changes part'.


-- With these definitions in place, we can now parse regular expressions.
--
-- REP> parse re "(a+b)*"
-- Just (Repeat (Plus (Symbol 'a') :* Symbol 'b'))
-- REP> parse re "(ab|cd*)f"
-- Just (((Symbol 'a' :* Symbol 'b') :+ (Symbol 'c' :* Repeat (Symbol 'd'))) :* Symbol 'f')
-- REP> parse re "*"
-- Nothing




-- Arithmetic Expressions
-- ----------------------

-- Our next example is arithmetic expressions involving precedence and
-- non-associativity. The AST we will produce is:

data Expr
    = Var Char
    | Int Integer
    | Add Expr Expr
    | Sub Expr Expr
    | Mul Expr Expr
    deriving Show

-- You can write an interpreter fairly easily, if you like. It will probably
-- have a type like interp :: (Char -> Int) -> Expr -> Int

-- The grammar is conveniently expressed like so:
--
--     Expr ::= Expr + Term
--     Expr ::= Expr - Term
--     Expr ::= Term
--
--     Term ::= Term * Factor
--     Term ::= Factor
--
--     Factor ::= ( Expr )
--     Factor ::= Integer
--     Factor ::= Variable
--
-- Unfortunately, this definition is left recursive, so we will need to
-- rework it in order to use Parser. One possibility:
--
--     Expr ::= Term Terms
--
--     Terms ::= AddSub Term Terms
--     Terms ::=
--
--     AddSub ::= +
--     AddSum ::= -
--
--     Term ::= Factor Factors
--
--     Factors ::= * Factor Factors
--     Factors ::=
--
--     Factor ::= ( Expr )
--     Factor ::= Integer
--     Factor ::= Variable
--
-- As with bits, we could use many to get a list of operators and operands,
-- but instead we will have Terms and Factors produce functions, and pass the
-- value generated by Term or Factor to them.

-- Definitions for expr, term, and factor are simple.

expr = term <**> terms

term = factor <**> factors

factor = tok (ch '(') *> expr <* tok (ch ')')
    <|> Int <$> tok int
    <|> Var <$> tok (oneof ['A'..'Z'])

-- Note the use of tok :: Parser a -> Parser a, which adds the ability to skip
-- zero or more spaces preceeding the matched string. It can be defined using
-- the tools we already have,
--
--    tok p = many (ch ' ') *> p
--
-- The actual definition avoids creating an unneeded list of spaces.
--
-- We define two parsers for the operators themselves:

addsub = tok
    (   ch '+' *> pure Add
    <|> ch '-' *> pure Sub)

mult = tok (ch '*') *> pure Mul

-- terms and factors will produce a function that takes a term or factor that
-- occurs previously and creates a right-nested value.
--
-- For example, "- Y - Z" should produce \x -> Sub (Sub x (Var 'Y')) (Var 'Z')
--
-- The rule we want has the form Terms ::= AddSub Term Terms, so we will want
-- combining function that takes four arguments and produces an Expr. These are:
--
--   1. the function f :: Expr -> Expr -> Expr produced by AddSub
--   2. the value y :: Expr produced by Term
--   3. the function k :: Expr -> Expr produced by Terms
--   4. the value x :: Expr that occurred left of the Terms
--
-- we combine them into k (f x y), passing the expression created by f to the
-- function produced by Terms.
--
-- For the rule Terms ::= , we will produce a function that simply returns its
-- argument unchanged.

terms   = (\f y k x -> k (f x y)) <$> addsub <*> term   <*> terms
        <|> pure id

factors = (\f y k x -> k (f x y)) <$> mult   <*> factor <*> factors
        <|> pure id

-- PRE> parse expr "X + Y * Z + 2"
-- Just (Add (Add (Var 'X') (Mul (Var 'Y') (Var 'Z'))) (Int 2))
-- PRE> parse expr "X - Y - Z"
-- Just (Sub (Sub (Var 'X') (Var 'Y')) (Var 'Z'))
-- REP> parse expr "(X - Y) - Z"
-- Just (Sub (Sub (Var 'X') (Var 'Y')) (Var 'Z'))
-- REP> parse expr "X - (Y - Z)"
-- Just (Sub (Var 'X') (Sub (Var 'Y') (Var 'Z')))


-- Notice that term/terms and factor/factors have very similar structure.
-- Rather than write this from scratch each time, we can abstract over the
-- specific operator and subterm and create a general function that will
-- make parsers for left associative operators.
--
-- lbinop o p is a parser that recognizes one or more substrings matching p,
-- separated by substrings matching o, and applies the functions produced by
-- o from left to right.

lbinop :: Parser (a -> a -> a) -> Parser a -> Parser a
lbinop op st = st <**> sts
    where
    sts = (\f y k x -> k (f x y)) <$> op <*> st <*> sts
        <|> pure id

-- We then could redefine expr and term using lbinop:
--
-- expr = lbinop addsub term
-- term = lbinop mult factor
--
-- Or, in one shot:

expr' =
    lbinop addsub
        (lbinop mult
            (   tok (ch '(') *> expr' <* tok (ch ')')
            <|> Int <$> tok int
            <|> Var <$> tok (oneof ['A'..'Z'])
            )
        )


-- We can define a function for right-associative operators as well.

rbinop :: Parser (a -> a -> a) -> Parser a -> Parser a
rbinop op st = st <**> sts
    where
    sts = (\f y k x -> f x (k y)) <$> op <*> st <*> sts
        <|> pure id

-- Unlike the hand-written operators used for re and choice, this only parses
-- the first non-terminal once.

-- We can also define a function to produce postfix operators.

postop :: Parser (a -> a) -> Parser a -> Parser a
postop op st = st <**> ops
    where
    ops = flip (.) <$> op <*> ops
        <|> pure id

-- We can then rewrite re using rbinop and postop. Let's further parameterize it
-- over the parser for symbols.

gre :: Parser a -> Parser (RE a)
gre sp =
    rbinop (ch '|' *> pure (:+))
        (rbinop (pure (:*))
            (postop star
                (   ch '(' *> gre sp <* ch ')'
                <|> ch 'e' *> pure Empty
                <|> Symbol <$> sp)))



-- Parser
-- ------


-- The implementation of Parser is below.
--
-- A parser is a function that takes a string, and returns zero or more
-- values that could be produced from the string in different ways. This
-- simulates a non-deterministic choice by backtracking. Along with each
-- possible value, the function also returns the remaining unparsed portion of
-- the string. When combining parsers sequentially, the portion of the string
-- not parsed by the first parser will be parsed by the second parser.


newtype Parser a = Parser { runP :: String -> [(a, String)] }

parse :: Parser a -> String -> Maybe a
parse p s = case filter (null . snd) (runP p s) of
    (a,_):_ -> Just a
    _ -> Nothing

oneof :: [Char] -> Parser Char
oneof xs = Parser $ \s ->
    case s of
        c:cs | c `elem` xs -> [(c,cs)]
        _ -> []

ch :: Char -> Parser Char
ch x = Parser $ \s ->
    case s of
        c:cs | c == x -> [(c,cs)]
        _ -> []

ws :: Parser ()
ws = Parser $ \s -> [((), dropWhile (' ' ==) s)]

tok :: Parser a -> Parser a
tok p = ws *> p

instance Functor Parser where
    fmap f p = Parser $ \s -> [ (f x, s1) | (x, s1) <- runP p s ]
        -- for each value x produced by p, produce f x

instance Applicative Parser where
    pure x = Parser $ \s -> [(x,s)]
        -- return a value without consuming any input

    p <*> q = Parser $ \s0 ->
        [ (f x, s2) | (f, s1) <- runP p s0, (x, s2) <- runP q s1 ]
        -- for each function f produced by p, pass the unconsumed portion of
        -- the input to q; for each value x produced by q, return f x and any
        -- remaining unconsumed input

instance Alternative Parser where
    empty   = Parser $ \_ -> []
        -- always fails; neutral element for <|>

    p <|> q = Parser $ \s -> runP p s ++ runP q s
        -- combine the possible results for p and q on input s



-- >>= can be used to define context-dependent parsers. For CFGs, we can
-- stick to pure and <*>.

instance Monad Parser where
    return = pure

    m >>= f = Parser $ \s0 -> do
        (x,s1) <- runP m s0
        runP (f x) s1

instance MonadPlus Parser where
    mzero = empty
    mplus = (<|>)
