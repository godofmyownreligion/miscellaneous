:- use_module(library(clpfd)).


% slide #5
length1([], 0).
length1([_|L], N) :- N #= M + 1, length1(L, M).

% In this version of length/2, both clauses can succeed when the second
% parameter is 0. This leads to non-termination when the first parameter is
% unbound, as the search strategy will attempt to resolve length with negative
% values.


% slide #8
length2([], 0).
length2([_|L], N) :- N #> 0, N #= M + 1, length2(L, M).

% This version of length/2 constrains the second parameter to be non-negative.
% Attempts to resolve length2(L, 0) will terminate, but the search strategy
% will always try the second clause before determining that it cannot succeed.

% Note that we are still sensitive to the order in which the antecedents are
% given. It is important to require N #> 0 before trying to resolve the 
% recursive sub-goal, or else we end up with the same problem as length1/2.

length2a([], 0).
length2a([_|L], N) :- length2a(L, M), N #> 0, N #= M + 1.

% This is a situation where the reality of Prolog falls short of the promise
% of logic progamming. Other languages, such as Mercury, allow the environment
% to resolve sub-goals in an optimal order, instead of left-to-right.


% slide #10
length3(L, N) :- zcompare(Cmp, N, 0), length3(Cmp, N, L).

length3(=, 0, []).
length3(>, N, [_|L]) :- N #= M + 1, length3(L, M).

% This version of length/2 uses zcompare/3 to ensure that at most one clause
% of the helper relation length3/3 will be attempted.


% slide #9
length3a(L, N) :- zcompare(Cmp, N, 0), length3a(Cmp, L, N).

length3a(=, [], 0).
length3a(>, [_|L], N) :- N #= M + 1, length3a(L, M).

% This version of length/2 should be equivalent to length3/2.
