:- use_module(library(dif)).

road('New York', 'Philadelphia').
road('New York', 'Boston').
road('Albany', 'Boston').
road('Albany', 'Pittsburgh').
road('Philadelphia', 'Pittsburgh').
road('Philadelphia', 'Washington').

roads(S, D) :- road(S, D).
roads(S, D) :- road(D, S).


route(S, S, [S]).
route(S, D, [S|P]) :- roads(S, X), route(X, D, P).



nonmember(_, []).
nonmember(X, [E|Es]) :- dif(X, E), nonmember(X, Es).


routeb(S, S, [S]).
routeb(S, D, [S|P]) :- roads(S, X), routeb(X, D, P), nonmember(S, P).


routec(S, D, [S|P]) :- routec([S], S, D, P).

routec(_, S, S, []).
routec(Seen, S, D, [X|P]) :-
	roads(S, X),
	nonmember(X, Seen),
	routec([X|Seen], X, D, P).

