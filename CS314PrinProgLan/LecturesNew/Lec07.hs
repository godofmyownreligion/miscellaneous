module Lec07 where

without_r e [] = Nothing
without_r e (x:xs)
    | e == x    = Just xs
    | otherwise =
        case without_r e xs of
            Nothing -> Nothing
            Just ys -> Just (x:ys)

without_f e xs = fst (foldr f (Nothing,[]) xs)
    where
    f x (_,xs) | x == e = (Just xs,     x:xs)
    f x (Nothing, xs)   = (Nothing,     x:xs)
    f x (Just ys, xs)   = (Just (x:ys), x:xs)

data Tree a = Tip | Bin (Tree a) a (Tree a) deriving (Show, Eq)

height Tip = 0
height (Bin l x r) = 1 + max (height l) (height r)

treeSum Tip = 0
treeSum (Bin l x r) = treeSum l + x + treeSum r

bst_search e Tip         = False
bst_search e (Bin l x r) =
    case compare e x of
        LT -> bst_search e l
        EQ -> True
        GT -> bst_search e r

hasDup Tip         = False
hasDup (Bin l x r) =
    hasDup l || hasDup r || treeMax l == Just x || treeMin r == Just x

treeMax Tip           = Nothing
treeMax (Bin _ x Tip) = Just x
treeMax (Bin _ _ r)   = treeMax r

treeMin Tip           = Nothing
treeMin (Bin Tip x _) = Just x
treeMin (Bin l   _ _) = treeMin l
