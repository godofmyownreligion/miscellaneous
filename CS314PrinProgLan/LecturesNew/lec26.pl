bplist00�_WebMainResource�	
^WebResourceURL_WebResourceFrameName_WebResourceData_WebResourceMIMEType_WebResourceTextEncodingName_mhttps://content.sakai.rutgers.edu/access/content/group/54c98b11-5527-41e8-a05f-29c3b9698b9d/Lectures/lec26.plPO"8<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">:- use_module(library(clpfd)).

% rule(+Head, +Body) - defines a clause in our interpreted language
%
% Head is a term that we are defining in the language.
% Body gives antecedents for Head.
%
% To simplify the interpreter, we will make a distinction between terms that
% are part of the language and terms that are defined within the language. That
% is, we will distinguish terms that are defined using rules (such as parent/2)
% and those which are provided by the interpreter (such as dif/2).
%
% We arbitrarily choose to indicate defined terms using {}.

rule(parent(jim, joe),  true).
rule(parent(joan, joe), true).
rule(parent(jack, jim), true).
rule(parent(jane, joan), true).

rule(sibling(X, Y), (dif(X,Y), {parent(X, P)}, {parent(Y, P)})).

rule(cousin(A, B), ({parent(A, C)}, {parent(B,D)}, {sibling(C, D)})).

rule(ancestor(A,B), {parent(A, B)}).
rule(ancestor(A,B), ({parent(A, C)}, {ancestor(C,B)})).
	% note that having multiple rules with the same head is fine


% interp(+Goal)  - interprets Goal in our interpreted language

interp(true).
interp(X = X).
interp(dif(X,Y)) :- dif(X,Y).
interp((P,Q))    :- interp(P), interp(Q).
interp((P;Q))    :- interp(P); interp(Q).
interp({G})      :- rule(G, Body), interp(Body).


:- op(750, xfy, &lt;=).

% justify(+Goal, -Justification)  - interprets Goal in the interpreted language
%   returns its reasoning in Justification
%
% ?- justify({ancestor(jane, joe)}, J).
% J = [ ancestor(jane, joe)&lt;=
%         [ parent(jane, joan)&lt;=[]
%         , ancestor(joan, joe)&lt;=[ parent(joan, joe)&lt;=[] ]
%         ]
%     ] .


justify(Goal, J) :- justify(Goal, J, []).

justify(true,  J, J).
justify(X = X, J, J).
justify(dif(X,Y), [dif(X,Y)|J], J) :- dif(X,Y).
justify((P,Q), J, J0) :-
	justify(P, J, J1),
	justify(Q, J1, J0).
justify((P;Q), J0, J) :-
	justify(P, J0, J);
	justify(Q, J0, J).
justify({G}, [G &lt;= JG | J], J) :-
	rule(G, Body),
	justify(Body, JG, []).
	


% pr_clause(+Head, +Body, +Sources) - defines a clause with provenance in an
%     interpreted language
%
% Head is a term
% Body gives the antecedents
% Sources is a list of sources (empty for obvious truths)
%
% Using this, we can determine what sources must be trusted in order to rely
% on a conclusion.

pr_clause(parent(jim, joe),   true, []).
pr_clause(parent(jane, jim),  true, []).
pr_clause(parent(john, jane), true, [rumor]).
pr_clause(parent(jack, john), true, []).

pr_clause(ancestor(X,Y), ({parent(X,P)}, (P = Y; {ancestor(P, Y)})), []).

pr_clause(friend(alice, bob),    true, [alice]).
pr_clause(friend(bob, carol),    true, [bob]).
pr_clause(friend(bob, eve),      true, [bob]).
pr_clause(friend(eve, bob),      true, [eve]).
pr_clause(friend(carol, daniel), true, [carol]).
pr_clause(friend(daniel, eve),   true, [rumor]).

pr_clause(friends(X, Y), {friend(X, Y)}, []).
pr_clause(friends(X, Y), {friend(Y, X)}, []).

pr_clause(contact(X, Y), path(friends, X, Y, _), []).
	% our language isn't quite powerful enough to define path/4, so we will have
	% the interpreter handle it
	%
	% contrast with ancestor/2, which is defined in our language


% pr_call(+Goal, -Sources) - interprets Goal in the interpreted language;
%
% Any sources that must be trusted will be included in Sources
%
% ?- pr_call({ancestor(jack, jane)}, S).
% S = [rumor] .
%
% ?- pr_call({friends(bob, eve)}, S).
% S = [bob] ;
% S = [eve] .
%
% ?- pr_call({contact(bob,daniel)}, S), nonmember(bob, S).
% S = [eve, rumor] .
% 
% ?- pr_call(path(friends, bob, daniel, P), S).
% P = [bob, carol, daniel],
% S = [bob, carol] ;
% P = [bob, eve, daniel],
% S = [bob, rumor] ;
% P = [bob, eve, daniel],
% S = [eve, rumor] ;
% false.


pr_call(Goal, Sources) :- pr_call(Goal, [], Sources).

% pr_call(+Goal, +Sources1, -Sources2) - interprets Goal
%
% Sources1 must be an ordered list of sources;
% Sources2 will be the union of Sources1 and the sources needed to prove Goal.

pr_call(true,     S, S).
pr_call(X = X,    S, S).
pr_call(dif(X,Y), S, S) :- dif(X, Y).

pr_call((P,Q), S0, S) :-
	pr_call(P, S0, S1),
	pr_call(Q, S1, S).

pr_call((P;Q), S0, S) :-
	pr_call(P, S0, S);
	pr_call(Q, S0, S).

pr_call({G}, S0, S) :-
	pr_clause(G, Body, Src),
	sort(Src, S1),
	ord_union(S0, S1, S2),
	pr_call(Body, S2, S).

pr_call(path(Rel, A, Z, [A|P]), S0, S) :-
	pr_path(Rel, A, Z, P, S0, S).

% pr_path(:Rel, ?Start, ?Finish, ?Path, +Sources1, -Sources2)

pr_path(_, A, A, [], S, S).
pr_path(Rel, A, Z, [B|Rest], S0, S) :-
	nonmember(A, [B|Rest]),
	append_args(Rel, A, B, Goal),
	pr_call({Goal}, S0, S1),
	pr_path(Rel, B, Z, Rest, S1, S).


append_args(T0, X, Y, T) :-
	T0 =.. L0,
	append(L0, [X,Y], L),
	T =.. L.

nonmember(_, []).
nonmember(X, [E|Es]) :-
	dif(X,E),
	freeze(Es, nonmember(X, Es)).



% Instead of an interpreter, we can make a meta-interpreter: a Prolog
% interpreter written in Prolog.

parent(john, joe).
parent(jim, john).
parent(jill, john).
parent(jean, joe).
parent(jack, jean).
parent(jane, jean).

sibling(X, Y) :-
	dif(X, Y),
	parent(X, P),
	parent(Y, P).

ancestor(D,A) :- 
	parent(D, P),
	(P = A; ancestor(P, A)).

friend(alice, bob).
friend(bob, carol).
friend(bob, daniel).
friend(carol, eve).
friend(eve, daniel).
%friend(X, Y) :- friend(Y, X).

friends(X, Y) :- friend(X, Y).
friends(X, Y) :- friend(Y, X).




% mi_clause(+Goal, -Body) 

mi_clause(Goal, Body) :-
	clause(Goal, Subgoals),
	clean(Subgoals, Body).

clean(true, true).
clean(X = Y, X = Y).
clean(dif(X,Y), dif(X,Y)).
clean((P,Q), (CP,CQ)) :- clean(P, CP), clean(Q, CQ).
clean((P;Q), (CP;CQ)) :- clean(P, CP), clean(Q, CQ).
clean(mi_clause(G,B), mi_clause(G,B)).
clean(Goal, {Goal}) :-
	Goal \= (_,_),
	Goal \= (_;_),
	Goal \= (_ = _),
	Goal \= dif(_,_),
	Goal \= true,
	Goal \= mi_clause(_,_).

mi_justify(G, J) :- mi_justify(G, J, []).

mi_justify(true, J, J).
mi_justify(X = X, J, J).
mi_justify(dif(X,Y), [dif(X,Y)|J], J).
mi_justify((P,Q), J, J0) :-
	mi_justify(P, J, J1),
	mi_justify(Q, J1, J0).
mi_justify((P;Q), J, J0) :-
	mi_justify(P, J, J0);
	mi_justify(Q, J, J0).
mi_justify(mi_clause(G,B), [mi_clause(G,B)|J], J) :- mi_clause(G,B).
mi_justify({Goal}, [Goal &lt;= JB|J0], J0) :- 
	mi_clause(Goal, Body),
	mi_justify(Body, JB).



limited(Goal, Max) :- limited(Goal, Max, _).

limited(true, N, N).
limited(X = X, N, N).
limited(dif(X,Y), N, N) :- dif(X,Y).
limited((P,Q), N0, N) :-
	limited(P, N0, N1),
	limited(Q, N1, N).
limited((P;Q), N0, N) :-
	limited(P, N0, N);
	limited(Q, N0, N).
limited({G}, N0, N) :-
	N0 #&gt; 0,
	N1 #= N0 - 1,
	mi_clause(G, B),
	limited(B, N1, N).



bad_mergesort([], []).
bad_mergesort([X], [X]).
bad_mergesort([X,Y|Unsorted], Sorted) :-
    partition(Unsorted, L1, L2),
    bad_mergesort([X|L1], SL1),
    bad_mergesort([Y|L2], SL2),
    bad_merge(SL1, SL2, Sorted).


bad_merge([], Ys, Ys).
bad_merge([X|Xs], [], [X|Xs]).
bad_merge([X|Xs], [Y|Ys], Zs) :- zcompare(C,X,Y), bad_merge(C,X,Xs,Y,Ys,Zs).

bad_merge(&lt;, X, Xs, Y, Ys, [X|Zs]) :- bad_merge(Xs, [Y|Ys], Zs).
bad_merge(=, X, Xs, Y, Ys, [X|Zs]) :- bad_merge(Xs, [Y|Ys], Zs).
bad_merge(&gt;, X, Xs, Y, Ys, [Y|Zs]) :- bad_merge([X|Xs], Ys, Zs).





mergesort([], []).
mergesort([X|Xs], Sorted) :- mergesort(Xs, X, Sorted).

mergesort([], X, [X]).
mergesort([Y|Unsorted], X, Sorted) :-
    partition(Unsorted, L1, L2),
    mergesort(L1, X, SL1),
    mergesort(L2, Y, SL2),
    merge(SL1, SL2, Sorted).

partition([], [], []).
partition([X|Xs], [X|Ys], Zs) :-
	partition(Xs, Zs, Ys).


merge([], Ys, Ys).
merge([X|Xs], Ys, Zs) :- merge(Ys, X, Xs, Zs).

merge([], X, Xs, [X|Xs]).
merge([Y|Ys], X, Xs, Zs) :- 
	X #=&lt; Y #&lt;==&gt; B,
	merge(B,X,Xs,Y,Ys,Zs).

merge(1,X,Xs,Y,Ys,[X|Zs]) :- merge(Xs,Y,Ys,Zs).
merge(0,X,Xs,Y,Ys,[Y|Zs]) :- merge(Ys,X,Xs,Zs).




my_once(G) :- call(G), !.    % danger!


% -----

% permutation(+List, -PermutedList)
permutation([], []).
permutation(Xs, [E|Es]) :-
    select(E, Xs, Ys),
    permutation(Ys, Es).

ordered([]).
ordered([E|Es]) :- ordered(Es, E).

ordered([], _).
ordered([E|Es], X) :- X #=&lt; E, ordered(Es, E).


slowsort(List, Sorted) :-
    permutation(List, Sorted),
    ordered(Sorted).

slowsort2(List, Sorted) :-
    ordered(Sorted),
    permutation(List, Sorted).


same_length([], []).
same_length([_|A], [_|B]) :- same_length(A, B).

slowsort3(List, Sorted) :-
    same_length(List, Sorted),
    ordered(Sorted),
    permutation(List, Sorted).

% How are slowsort, slowsort2, and slowsort3 different?
</pre><div id="UMS_TOOLTIP" style="position: absolute; cursor: pointer; z-index: 2147483647; background-color: transparent; top: -100000px; left: -100000px; background-position: initial initial; background-repeat: initial initial;"></div></body><umsdataelement id="UMSSendDataEventElement"></umsdataelement></html>[text/x-perlUUTF-8    ( 7 N ` v �#A#M                           #S